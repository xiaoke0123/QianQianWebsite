from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination, CursorPagination


# """合作案例的分页功能"""
class MyPagination(PageNumberPagination):
    # xxxx?page=1&size=2
    page_size = 9
    page_query_param = "page"
    page_size_query_param = "size"
    max_page_size = 9
    # est_framework.response.Response


# """新闻资讯分页功能"""
class MyNewsInfoPagenation(PageNumberPagination):
    # xxxx?page=1&size=2
    page_size = 4
    page_query_param = "page"
    page_size_query_param = "size"
    max_page_size = 10


# '''单条资讯详情和单条案例详情的页面分类'''
class MySinglePagination(PageNumberPagination):
    # xxxx?page=1&size=2
    page_size = 1
    page_query_param = "page"
    page_size_query_param = "size"
    max_page_size = 1


# """合作案例的分页功能"""
class MyCasePagination(LimitOffsetPagination):#
    default_limit = 8
    limit_query_param = "limit"
    offset_query_param = "offset"
    # max_limit = 9


# """新闻资讯的分页功能"""
class NewsINfoPagination(LimitOffsetPagination):#
    default_limit = 4
    limit_query_param = "limit"
    offset_query_param = "offset"
    # max_limit = 6


# class MyPagination(CursorPagination):#
#     cursor_query_param = "cursor"
#     page_size = 2
#     ordering = "-id"






