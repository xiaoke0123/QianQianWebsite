# -*- coding: utf-8 -*-
# author: yujc
# file: middleware.py
# time: 2019/11/18 13:56

from django.middleware.security import SecurityMiddleware
from django.utils import deprecation
from rest_framework.response import Response


class MyCrosMiddleware(deprecation.MiddlewareMixin):
    # @staticmethod
    # def process_request(request):
    #     if request.method.lower() == 'options':
    #         return Response('ok')
    origin_list = ["http://192.168.0.135:8080"]

    def process_response(self, request, response):
        # response["Access-Control-Allow-Origin"] = "http://192.168.0.135"
        response["Access-Control-Allow-Origin"] = request.META.get("Access-Control-Allow-Origin") \
            if request.META.get("Access-Control-Allow-Origin") in self.origin_list else '*'
        response["Access-Control-Allow-Method"] = request.META.get("Access-Control-Allow-Method")
        return response


"""
class MyCrosMiddleware(deprecation.MiddlewareMixin):
    # @staticmethod
    def process_response(request, response):
        response["Access-Control-Allow-Origin"] = '*'
        if request.method == "OPTIONS":
            response["Access-Control-Allow-Methods"] = " PUT, POST, DELETE"
            response["Access-Control-Allow-Headers"] = "content-type"
        return response
        """
'''
from django.middleware.security import SecurityMiddleware
from django.utils.deprecation import MiddlewareMixin

class MyCors(MiddlewareMixin):
    def process_response(self, request, response):
        response["Access-Control-Allow-Origin"] = "*"
        if request.method == "OPTIONS":
            response["Access-Control-Allow-Methods"] = "PUT, DELETE"
            response["Access-Control-Allow-Headers"] = "content-type"
        return response
'''