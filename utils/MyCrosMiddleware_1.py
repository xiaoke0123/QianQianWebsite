# -*- coding: utf-8 -*-
# author: yujc
# file: middleware.py
# time: 2019/11/18 13:56
from django.utils import deprecation
from django.middleware.security import SecurityMiddleware
# from django.utils.deprecation import MiddlewareMixin

# file: middleware.py
# time: 2019/11/18 13:56

# 第一次原始的
# from django.utils import deprecation
# class MyCrosMiddleware(deprecation.MiddlewareMixin):
#     @staticmethod
#     def process_response(request, response):
#         response["Access-Control-Allow-Origin"] = '*'
#         return response



class MyCrosMiddleware(deprecation.MiddlewareMixin):
    # @staticmethod
    def process_response(self, request, response):
        response["Access-Control-Allow-Origin"] = "*"
        if request.method == "OPTIONS":
            response["Access-Control-Allow-Methods"] = "PUT, POST, DELETE"
            response["Access-Control-Allow-Headers"] = "content-type"
        return response


"""
class MyCrosMiddleware(deprecation.MiddlewareMixin):
    # @staticmethod
    def process_response(request, response):
        response["Access-Control-Allow-Origin"] = '*'
        if request.method == "OPTIONS":
            response["Access-Control-Allow-Methods"] = " PUT, POST, DELETE"
            response["Access-Control-Allow-Headers"] = "content-type"
        return response
        """

'''
from django.middleware.security import SecurityMiddleware
from django.utils.deprecation import MiddlewareMixin


class MyCors(MiddlewareMixin):
    def process_response(self, request, response):
        response["Access-Control-Allow-Origin"] = "*"
        if request.method == "OPTIONS":
            response["Access-Control-Allow-Methods"] = "PUT, DELETE"
            response["Access-Control-Allow-Headers"] = "content-type"
        return response
'''