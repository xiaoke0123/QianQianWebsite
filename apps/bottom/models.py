from django.db import models
from django.contrib.auth.models import AbstractUser, Group
from django.db import models
from tinymce.models import HTMLField
# Create your models here.
from QianQianWebsite.settings import IP_PORT
from django.shortcuts import render, redirect, HttpResponse


# """底部导航之类目分类"""
class BottomCategory(models.Model):
    case_category = models.ForeignKey('self', max_length=2, null=True, blank=True, verbose_name='所属案例类目')
    classify_name = models.CharField(max_length=40, null=True, blank=True, verbose_name='分类名称')
    url = models.CharField(max_length=40, null=True, blank=True, verbose_name='跳转链接')
    index = models.SmallIntegerField(null=True, blank=True, default=0, verbose_name='排序')

    class Meta:
        # db_table = 'BottomCategory'
        db_table = 'bottomcategory'
        verbose_name = '底部类目'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.classify_name