# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2019-11-20 17:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bottom', '0003_auto_20191117_1742'),
    ]

    operations = [
        migrations.AddField(
            model_name='bottomcategory',
            name='index',
            field=models.SmallIntegerField(blank=True, default=0, null=True, verbose_name='排序'),
        ),
    ]
