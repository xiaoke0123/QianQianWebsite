# from extra_apps import xadmin
# from extra_apps.xadmin import views
from django.shortcuts import render, redirect, HttpResponse
import xadmin
from xadmin import views

from .models import BottomCategory

# Register your models here.


# """底部导航之-类目分类"""
class BottomCategoryAdmin(object):
    # model_icon = 'fa fa-envelope'
    list_per_page = 20
    show_bookmarks = False  # 关闭书签
    list_display = ['id', 'case_category', 'classify_name', 'url', 'index']
    list_editable = ['index', 'classify_name', 'case_category', 'url']
    ordering = ('case_category', 'index', 'classify_name',)
    search_fields = ['index', 'classify_name', 'case_category', 'url']
    list_filter = ['case_category', ]

    # 服务分类的多对多字段过滤
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'case_category':
            kwargs["queryset"] = BottomCategory.objects.filter(case_category__isnull=True)
        return super(BottomCategoryAdmin, self).formfield_for_dbfield(db_field, **kwargs)

xadmin.site.register(BottomCategory, BottomCategoryAdmin)



# class CandidateAdmin(object):
#     # 检索字段
#     search_fields = ['case_category', 'classify_name', 'cTimes', 'cVotes', 'cPinyin', 'isDelete', ]
#     # 要显示的字段
#     # list_display = ['id', 'cName', 'cAge', 'cEmail', 'cDeclaration', 'cIcon', 'cTimes', 'cVotes', 'cPinyin', 'isDelete', 'cVoteType',]
#     # 分组过滤的字段
#     # list_filter = ['cName', 'cAge', 'cTimes', 'cVotes', 'cPinyin', 'isDelete', ]
#     # ordering设置默认排序字段，负号表示降序排序
#     ordering = ('id',)
#     # fk_fields 设置显示外键字段
#     fk_fields = ('cVoteType',)
#     # 当一些字段只适合查看的时候，就需要在模型注册类中做出如下添加
#     readonly_fields = ['fav_nums', 'click_num', 'students']
#     list_editable = ['degree', 'desc']  # 列表页直接编辑修改
#     exclude = ['', ]  # 不显示字段
#     refresh_times = [3, 5]  # 计时刷新