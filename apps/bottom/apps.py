from django.apps import AppConfig


class BottomConfig(AppConfig):
    name = 'apps.bottom'
    verbose_name = '底部导航'
