from rest_framework import serializers
import re, time
from django.db.models import Q

from .models import BusinessIntroduce, WashCar, TechDevp, Advertise, CompanyIntroduce, CompanyInfo, CompanyService, \
    ImgWords
from apps.advertisement.models import ManageImg, Partner
from apps.bottom.models import BottomCategory
from cooperate_case.models import CaseList, CaseClassify
from form_manage.models import FormsManage
from news.models import InfoClassify, InfoList
from system.models import SiteConfiguration, User
from QianQianWebsite.settings import IP_PORT

# class imgSerializer(serializers.Serializer):
#     def __init__(self, image):
#         self.img = image
#     img = serializers.SerializerMethodField(read_only=True)
#     def get_pc_img_01(self, obj):
#         return ("%s%s" % (settings.IP_PORT, obj.pc_img_01))


# '''业务介绍页面钳钳洗车视频的序列化'''
class WashCarVideoSerializer(serializers.Serializer):
    video_background = serializers.SerializerMethodField(read_only=True)

    def get_video_background(self, obj):
        background_image = ImgWords.objects.filter(img_title='钳钳洗车视频背景图', belong_page=6).first().icon
        return ("%s%s" % (IP_PORT, background_image))

    video = serializers.SerializerMethodField(read_only=True)

    def get_video(self, obj):
        return ("%s%s" % (IP_PORT, obj.video))

    video_words = serializers.SerializerMethodField(read_only=True)

    def get_video_words(self, obj):
        video_words = ImgWords.objects.filter(img_title='钳钳洗车视频背景图', belong_page=6).first().services
        return (video_words)


# '''业务介绍页面钳钳洗车的序列化'''
class WashCarSerializer(serializers.ModelSerializer):
    service_img = serializers.SerializerMethodField(read_only=True)

    def get_service_img(self, obj):
        return ("%s%s" % (IP_PORT, obj.service_img))

    service_img2 = serializers.SerializerMethodField(read_only=True)

    def get_service_img2(self, obj):
        return ("%s%s" % (IP_PORT, obj.service_img2))

    class Meta:
        model = WashCar
        # fields = "__all__"  # 全部字段都序列化,不排序,如果要排序,用fields方法
        fields = ['service_img', 'title', 'title_desc', 'en_name', 'service_img2',]


# '''01首页广告设计的序列化'''
class AdvertiseSerializer(serializers.ModelSerializer):
    service_img = serializers.SerializerMethodField(read_only=True)

    def get_service_img(self, obj):
        return ("%s%s" % (IP_PORT, obj.service_img))
    class Meta:
        model = Advertise
        fields = ["title", "service_img",'id']


# '''首页技术开发的序列化'''
class TechDevpSerializer(serializers.ModelSerializer):
    class Meta:
        model = TechDevp
        fields = "__all__"  # 全部字段都序列化,不排序,如果要排序,用fields方法
        # fields = ['id',  ]


# '''4个公司地址及信息的序列化'''
class CompanyInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanyInfo
        fields = "__all__"


# '''首页公司简介的序列化'''
class CompanyIntroduceSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField(read_only=True)

    def get_image(self, obj):
        return ("%s%s" % (IP_PORT, obj.image))

    background_image = serializers.SerializerMethodField(read_only=True)

    def get_background_image(self, obj):
        background_image = ImgWords.objects.filter(img_title='03_企业介绍_top', belong_page=6).first().icon
        return ("%s%s" % (IP_PORT,background_image))

    class Meta:
        model = CompanyIntroduce
        fields = ['title', 'title_desc', 'desc', 'image', 'background_image', ]


# '''页面管理的序列化类'''
class ManageImgSerializer(serializers.ModelSerializer):
    pc_img = serializers.SerializerMethodField(read_only=True)
    mobile_img = serializers.SerializerMethodField(read_only=True)

    def get_pc_img(self,obj):
        return ("%s%s" %(IP_PORT, obj.pc_img))

    def get_mobile_img(self, obj):
        return ("%s%s" % (IP_PORT, obj.mobile_img))

    class Meta:
        model = ManageImg
        fields = "__all__"


#  '''首页我们的服务的序列化'''
class CompanyServiceSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField(read_only=True)

    def get_image(self, obj):
        return ("%s%s" % (IP_PORT, obj.image))

    class Meta:
        model = CompanyService
        # fields = "__all__"  # 全部字段都序列化,不排序,如果要排序,用fields方法
        fields = ["id", 'title', 'title_desc', 'en_name', 'image', 'desc']
        # fields = ['image']


#  '''首页之关于钳钳的序列化'''
class AboutQianQianSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField(read_only=True)

    def get_image(self, obj):
        return ("%s%s" % (IP_PORT, obj.image))

    class Meta:
        model = CompanyService
        # fields = "__all__"  # 全部字段都序列化,不排序,如果要排序,用fields方法
        fields = ["id", 'title', 'title_desc', 'en_name', 'image', 'desc']


#  '''业务介绍页面的我们的服务的序列化'''
class ImgWordsSerializer(serializers.ModelSerializer):
    icon = serializers.SerializerMethodField(read_only=True)

    def get_icon(self, obj):
        return ("%s%s" %(IP_PORT, obj.icon))

    # CHOICES = ((1, '业务介绍页面'), (2, '技术开发服务页面'), (3, '企业介绍'))
    # belong_page = serializers.ChoiceField(choices=CHOICES, source="get_belong_page_display", read_only=True)

    class Meta:
        model = ImgWords
        fields = ['icon', 'img_title', 'services']  # , 'belong_page'


#  '''05 技术开发服务页 应用场景 的序列化'''
class ScenarioSerializer(serializers.ModelSerializer):
    icon = serializers.SerializerMethodField(read_only=True)

    def get_icon(self, obj):
        if obj.icon:
            return ("%s%s" %(IP_PORT, obj.icon))
        else:
            return ''

    background_image = serializers.SerializerMethodField(read_only=True)

    def get_background_image(self, obj):
        if obj.background_image:
            return ("%s%s" %(IP_PORT, obj.background_image))
        else:
            return ''

    class Meta:
        model = ImgWords
        fields = ['icon', 'img_title', 'services', 'background_image']


# '''首页合作案例的序列化6张图'''
class CaseListSerializer(serializers.ModelSerializer):
    img = serializers.SerializerMethodField(read_only=True)

    def get_img(self, obj):
        return ("%s%s" % (IP_PORT, obj.img))

    url = serializers.SerializerMethodField(read_only=True)

    def get_url(self, obj):
        return ('case_detail/' + str(obj.id))

    case_time = serializers.SerializerMethodField(read_only=True)

    def get_case_time(self, obj):
        array = time.strptime(str(obj.case_time), u"%Y-%m-%d")
        return time.strftime("%d/%m/%Y", array)

    class Meta:
        model = CaseList
        fields = ['id', 'img',  "case_time", 'case_name', 'url', 'company_name', 'service_type']


# class ImageSerializer(serializers.Serializer):
#     def __init__(self, image):
#         self.image = image
#
#     icon = serializers.SerializerMethodField(read_only=True)
#
#     def get_icon(self, obj):
#         return ("%s%s" % (settings.IP_PORT, obj.icon))


# '''07单条带id的案例详情的序列化'''
class SingleCaseDetailSerializer(serializers.ModelSerializer):
    all_obj = CaseList.objects.all()
    next_id = serializers.SerializerMethodField()
    previous_id = serializers.SerializerMethodField(read_only=True)
    service_type = serializers.SerializerMethodField(read_only=True)
    industry_classify = serializers.SerializerMethodField(read_only=True)
    # industry_classify = serializers.ChoiceField(choices=service_type.types, source="get_industry_classify_display")
    # background_image = serializers.SerializerMethodField(read_only=True)

    # def get_background_image(self, obj):
    #     img_url = ImgWords.objects.filter(img_title='合作案例_详情_top', belong_page=6).first().icon
    #     return ("%s%s" % (settings.IP_PORT, img_url))

    def get_service_type(self, obj):
        return obj.service_type.types

    def get_industry_classify(self, obj):
        return obj.industry_classify.types

    def get_next_id(self, obj):
        try:
            next_id = self.all_obj.filter(Q(service_type=obj.service_type) | Q(industry_classify=obj.industry_classify),
                                          id__gt=obj.id).order_by('id').first().id
        except Exception as e:
            return
        return (next_id)


    def get_previous_id(self, obj):
        try:
            previous_id = self.all_obj.filter(Q(service_type=obj.service_type) | Q(industry_classify=obj.industry_classify),
                                              id__lt=obj.id).order_by('-id').first().id
        except Exception as e:
            return
        return (previous_id)

    class Meta:
        model = CaseList
        fields = ['industry_classify', 'company_name', 'service_type', 'function_dsc', 'next_id', 'previous_id', 'case_name'] # "id",


# '''合作案例_详情的序列化 9条数据'''
class CooperateCaseSerializer(serializers.ModelSerializer):
    all_obj = CaseList.objects.all()
    # next_url = serializers.SerializerMethodField(read_only=True)
    # previous_url = serializers.SerializerMethodField(read_only=True)

    service_type = serializers.SerializerMethodField(read_only=True)
    industry_classify = serializers.SerializerMethodField(read_only=True)
    img = serializers.SerializerMethodField(read_only=True)

    def get_img(self, obj):
        return ("%s%s" % (IP_PORT, obj.img))

    def get_service_type(self, obj):
        service_type_set = obj.service_type
        return service_type_set.types

    def get_industry_classify(self, obj):
        industry_classify_set = obj.industry_classify
        return industry_classify_set.types

    # def get_next_url(self, obj):
    #     try:
    #         next_id = self.all_obj.filter(service_type=obj.service_type, industry_classify=obj.industry_classify,
    #                                       id__gt=obj.id).order_by('id').first().id
    #     except Exception as e:
    #         return
    #     return ('case_detail/' + str(next_id))
    #
    # def get_previous_url(self, obj):
    #     try:
    #         previous_id = self.all_obj.filter(service_type=obj.service_type, industry_classify=obj.industry_classify,
    #                                           id__lt=obj.id).order_by('-id').first().id
    #     except Exception as e:
    #         return
    #     return ('case_detail/' + str(previous_id))

    class Meta:
        model = CaseList
        fields = ["id", 'industry_classify', 'service_type', 'function_dsc',  'case_name', 'img', 'company_name']  # 'next_url', 'previous_url',


# 12页 资讯页面公告分类和行业新闻"""  需要携带参数?category=1或2默认 category=2 公告 1是行业新闻   img
class NewsInfoSerializer(serializers.ModelSerializer):
    img = serializers.SerializerMethodField(read_only=True)

    def get_img(self, obj):
        return ("%s%s" % (IP_PORT, obj.img))


    abstract = serializers.SerializerMethodField(read_only=True)

    def get_abstract(self, obj):
        return (str(obj.content)[0:50] + "...")

    url = serializers.SerializerMethodField(read_only=True)

    def get_url(self, obj):
        return ('news_detail/'+ str(obj.id))

    class Meta:
        model = InfoList
        fields = ["id", "title", 'commit_time', 'url', 'abstract', 'img', 'category']


# '''首页新闻资讯的序列化'''
class InfoListSerializer(serializers.ModelSerializer):
    img = serializers.SerializerMethodField(read_only=True)

    def get_img(self, obj):
        return ("%s%s" % (IP_PORT, obj.img))

    class Meta:
        model = InfoList
        fields = "__all__"
        # fields = ['id', 'store_name', 'car_standard_price', "shop_lon"]


# '''企业介绍下的合作伙伴的序列化'''
class PartnerSerializer(serializers.ModelSerializer):
    partner_img = serializers.SerializerMethodField(read_only=True)

    def get_partner_img(self, obj):
        return ("%s%s" % (IP_PORT, obj.partner_img))

    class Meta:
        model = Partner
        # if request.path=
        fields = ["partner_img"]


# # """案例分类的序列化"""
# class CaseClassifySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = CaseClassify
#         fields = "__all__"


# """表单管理的序列化"""
class FormsManageSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormsManage
        fields = "__all__"


# """需求意向表的序列化"""
class DemandTableSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormsManage
        fields = ['type', 'company', 'name', 'telephone', "email"]  # "content",  , "content"
        # extra_kwargs = {"type": {"write_only": True}, "telephone": {"write_only": True}, "company": {"write_only": True}
        #                 , "email": {"write_only": True}, "content": {"write_only": True}, "name": {"write_only": True}}

    def validate_email(self, value):
        if not re.match(r'[a-z0-9][\w.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}', value):
            raise serializers.ValidationError({'errmsg': '邮箱格式不正确'})
        return value

    def validate_telephone(self, value):
        if not re.match(r"^1[35678]\d{9}$", value):
            raise serializers.ValidationError({'errmsg': '手机号码不正确'})
        return value

    def validate(self, attrs):
        my_set = set()
        for v in attrs.values():
            if len(str(v).strip()) == 0:
                my_set.add(0)
                raise serializers.ValidationError("表格没填完")
                # raise serializers.ValidationError({'errmsg': '表格没填完'})
            else:
                my_set.add(1)
        if all(my_set):
            return attrs


# """费用评估表的序列化"""
# class CostAccessSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = FormsManage
#         # fields = "__all__"  # 全部字段都序列化,不排序,如果要排序,用fields方法
#         fields = ['company', 'name', 'telephone', "email", "request", 'type', 'content']  #'w_type',
#
#         # extra_kwargs = {"service": {"write_only": True}, "budget": {"write_only": True}}
#
#     def validate_email(self, value):
#         if not re.match(r'[a-z0-9][\w.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', value):
#             raise serializers.ValidationError({'errmsg': '邮箱格式不正确'})
#         return value
#
#     def validate_telephone(self, value):
#         if not re.match(r"^1[35678]\d{9}$", value):
#             raise serializers.ValidationError({'errmsg': '手机号码不正确'})
#         return value
#
#     def validate(self, attrs):
#         my_set = set()
#         for v in attrs.values():
#             if len(v.strip()) == 0:
#                 my_set.add(0)
#                 raise serializers.ValidationError("表格没填完")
#             else:
#                 my_set.add(1)
#         if all(my_set):
#             return attrs


# """合作案例里面的费用评估表,表在FormsManage表单管理里面,只是获取表的提交,get方法无用""""""
class CostAccessSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormsManage
        fields = ['company', 'name', 'telephone', "email", 'type', 'content']  #  "request",

    def validate_email(self, value):
        if not re.match(r'[a-z0-9][\w.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', value):
            raise serializers.ValidationError({'errmsg': '邮箱格式不正确'})
        return value

    def validate_telephone(self, value):
        if not re.match(r"^1[35678]\d{9}$", value):
            raise serializers.ValidationError({'errmsg': '手机号码不正确'})
        return value

    def validate(self, attrs):
        my_set = set()
        for v in attrs.values():
            if len(str(v).strip()) == 0:
                my_set.add(0)
                raise serializers.ValidationError("表格没填完")
            else:
                my_set.add(1)
        if all(my_set):
            return attrs


# '''资讯详情页面4条数据的序列化'''
class InfoListSerializer(serializers.ModelSerializer):
    img = serializers.SerializerMethodField(read_only=True)

    def get_img(self, obj):
        return ("%s%s" % (IP_PORT, obj.img))

    abstract = serializers.SerializerMethodField(read_only=True)

    def get_abstract(self, obj):
        return (str(obj.content)[0:50] + "...")

    url = serializers.SerializerMethodField(read_only=True)

    def get_url(self, obj):
        return  ('news_detail/'+ str(obj.id))

    class Meta:
        model = InfoList
        fields = ["id", "title", 'commit_time', 'url', 'abstract', 'img', ]


# '''单条带id的资讯详情的序列化'''
class SingleNewsDetailSerializer(serializers.ModelSerializer):
    next_id = serializers.SerializerMethodField(read_only=True)
    previous_id = serializers.SerializerMethodField(read_only=True)
    all_obj = InfoList.objects.all()

    img = serializers.SerializerMethodField(read_only=True)

    def get_img(self, obj):
        return ("%s%s" % (IP_PORT, obj.img))

    img_01 = serializers.SerializerMethodField(read_only=True)

    def get_img_01(self, obj):
        return ("%s%s" % (IP_PORT, obj.img_01))

    img_02 = serializers.SerializerMethodField(read_only=True)

    def get_img_02(self, obj):
        return ("%s%s" % (IP_PORT, obj.img_02))

    def get_next_id(self, obj):
        try:
            next_id = self.all_obj.filter(category=obj.category, id__gt=obj.id).order_by('id').first().id
        except Exception as e:
            return
        # return ('news_detail/' + str(next_id))
        return (next_id)

    def get_previous_id(self, obj):
        try:
            previous_id = self.all_obj.filter(category=obj.category, id__lt=obj.id).order_by('-id').first().id
        except Exception as e:
            return
        # return ('news_detail/' + str(previous_id))
        return (previous_id)

    class Meta:
        model = InfoList
        fields = ["id", 'author', "title", 'commit_time', 'content', 'img', 'img_01', 'img_02', 'next_id', 'previous_id', 'category']


# '''合伙人计划获取数据的序列化'''
class PartnerInfoListSerializer(serializers.ModelSerializer):
    class Meta:
        model = SiteConfiguration
        fields = ['id', 'phone', 'WeChat_img', 'address', 'applet_img']


# """合伙人计划提交表的序列化"""
class PartnerTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = FormsManage
        fields = ['name', 'telephone', 'type']
        # extra_kwargs = {"service": {"write_only": True}, "budget": {"write_only": True}}

    def validate_telephone(self, value):
        if not re.match(r"^1[35678]\d{9}$", value):
            raise serializers.ValidationError({'errmsg': '手机号码不正确,请重新输入', })
            # raise serializers.ValidationError({'errmsg': '手机号码不正确,请重新输入', 'status':status.HTTP_400_BAD_REQUEST})
        return value

    def validate(self, attrs):
        my_set = set()
        for v in attrs.values():
            if len(str(v).strip()) == 0:
                my_set.add(0)
                raise serializers.ValidationError("表格没填完")
            else:
                my_set.add(1)
        if all(my_set):
            return attrs


# """底部导航表的序列化"""
class BottomSerializer(serializers.ModelSerializer):
    class Meta:
        model = BottomCategory
        fields = "__all__"  # 全部字段都序列化,不排序,如果要排序,用fields方法
        # fields = ['id', 'store_name', 'car_standard_price', "shop_lon", "shop_lat", ]
        # depth = 1


# """底部导航表的序列化"""
class BottomSerializer(serializers.ModelSerializer):
    class Meta:
        model = InfoList
        fields = "__all__"  # 全部字段都序列化,不排序,如果要排序,用fields方法
        # fields = ['id', 'store_name', 'car_standard_price', "shop_lon", "shop_lat", ]
        # depth = 1