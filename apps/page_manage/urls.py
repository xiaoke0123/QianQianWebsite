# -*- encoding: utf-8 -*-
from django.conf.urls import url
# from django.urls import path, re_path
from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include

from .views import IndexServiceView, WashCarView, CompanyIntroduceView, TechDevpView, AdvertiseView, \
    AboutUsView, CooperateCasePageView, CaseListView, DemandTableView, CostAccessTableView, NewsInfoView, \
    NewsRecommendView, SingleNewsDetailView, PartnerTableView, BottomView, PartnerView, CaseDetailView, ManageImgView, \
    ImgWordsView, IntroduceAdvertiseView, ServiceTechnologyView, ScenarioView, PreviousNextView, IntroduceServiceView, \
    IndexNewsView, TheoryView, AdProcessView, MobileIndexView, ServiceIndustryView, CaseBackgroundView, \
    CompanyAddressView, BackgroundRetrieveView, TimerView


router = DefaultRouter()
router.register(r"demand_table", DemandTableView, base_name='demand_table')

urlpatterns = [
    url(r'manage_img/$', ManageImgView.as_view(), name='manage_img'),    # 01首页轮播图
    url(r'index_service/', IndexServiceView.as_view(), name='our_service'),    # 01首页之我们的服务
    url(r'index_about/$', AboutUsView.as_view()),        # 01首页之关于钳钳
    url(r'mobile_index/$', MobileIndexView.as_view()),        # 01手机版首页
    url(r'case_list/$', CaseListView.as_view(), name='case_list'),    # 01首页下的6个案例
    url(r'index_news/$', IndexNewsView.as_view(), name='index_news'),    # 01首页下的4个新闻
    url(r'bottom/$', BottomView.as_view(), name='bottom'),  # 01底部页面显示

    url(r'introduce_service/$', IntroduceServiceView.as_view(), name='wash_car'),  # 02 业务介绍下的 我们的服务
    url(r'introduce_technology/$', TechDevpView.as_view(), name='technology'),       # 02页 业务介绍 之技术开发
    url(r'introduce_advertise/$', IntroduceAdvertiseView.as_view()),       # 02 业务介绍下的之广告设计

    url(r'company_introduce/$', CompanyIntroduceView.as_view()),          # 03企业介绍之 公司简介
    url(r'company_address/$', CompanyAddressView.as_view()),          # 03企业介绍之 公司简介的地址
    url(r'(?P<partner>03_partner|13_partner)/$', PartnerView.as_view()),  # 03企业介绍下合作伙伴 和13广告设计服务页下的

    url(r'service_industry/$', ServiceIndustryView.as_view()),  # 03 企业介绍下合作伙伴 和  13 广告设计服务页下的

    url(r'wash_car/$', WashCarView.as_view(), name='wash_car'),  # 04 关于钳钳之 钳钳洗车 有视频
    url(r'theory/$', TheoryView.as_view()),       # 04 关于钳钳之钳钳理念
    url(r'partner_commit/$', PartnerTableView.as_view(), name='partner_commit'),  # 04企业介绍下合伙人计划 合伙人计划提交表

    url(r'service_technology/$', ServiceTechnologyView.as_view()),   # 05技术开发服务页下的技术开发和流程
    url(r'scenario/$', ScenarioView.as_view()),     # 05技术开发服务页下的应用场景

    url(r'cooperate_page/$', CooperateCasePageView.as_view(), name='cooperate_page'),   # 06,09,10页  合作页面的案例的行业分类和服务分类
    url(r'case_background/$', CaseBackgroundView.as_view(), name='cooperate_page'),   # 06,09,10页 合作页面的案例的背景图

    url(r'background_retrieve/(?P<id>\d+)/$', BackgroundRetrieveView.as_view(), name='cooperate_page'),   # 06,09,10页 合作页面的案例的背景图

    url(r'case_detail/(?P<id>\d+)/$', CaseDetailView.as_view(), name='case_detail_id'),  # 07页  单独的某个id页面的案例详情
    # url(r'demand_table/$', DemandTableView.as_view()),        # 需求意向表
    # url(r'demand_table/$', DemandTableView.as_view({"post": "post"})),        # 需求意向表

    url(r'cost_access/$', CostAccessTableView.as_view()),        # 费用评估表

    url(r'news_info', NewsInfoView.as_view(), name='news_info'),  # 新闻资讯总页面的4条数据 公告和行业新闻,默认是公告
    url(r'news_detail/(?P<id>\d+)/$', SingleNewsDetailView.as_view(), name='news_detail'),  # 单独的某个id页面的资讯详情
    url(r'news_recommend/$', NewsRecommendView.as_view(), name='news_recommend'),   # 资讯详情页面右侧的推荐6个

    url(r'previous_next/(\d+)/(?P<classification>info_notice|info_news)/$', PreviousNextView.as_view(), name='previous_next'),  # 11页 资讯详情的上下篇
    url(r'advertise/$', AdvertiseView.as_view(), name='advertise'),   # 广告设计服务页之 广告设计的展示
    url(r'ad_process/$', AdProcessView.as_view(), name='ad_process'),   # 广告设计服务页之 流程的展示

    url(r'timer/$', TimerView.as_view()),       # 02 业务介绍下各个小图标和文字



    # url(r'img_word/$', ImgWordsView.as_view()),       # 02 业务介绍下各个小图标和文字
    # url(r'about_wash_car/$', WashCarView.as_view(), name='wash_car'),  # 钳钳洗车
    # url(r'^', include(router.urls)),

]

urlpatterns += router.urls