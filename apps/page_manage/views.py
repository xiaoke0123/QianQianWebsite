# -*- encoding: utf-8 -*-
from django.shortcuts import render, redirect, HttpResponse
from django.urls import reverse
# from django.core.paginator import Paginator      # 分页的
# from django.views.generic import View
from QianQianWebsite.settings import IP_PORT

import datetime, time
import threading
# from rest_framework.mixins import CreateModelMixin, UpdateModelMixin, RetrieveModelMixin
# from rest_framework import generics
# from rest_framework.pagination import PageNumberPagination, # 分页功能 , LimitOffsetPagination, CursorPagination
# from rest_framework.generics import GenericAPIView

from .models import BusinessIntroduce, WashCar, TechDevp, Advertise, CompanyIntroduce, CompanyInfo, \
    CompanyService, ImgWords, QianQianNumber

from rest_framework.viewsets import GenericViewSet, ModelViewSet
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from apps.advertisement.models import ManageImg, Partner
from apps.bottom.models import BottomCategory
from cooperate_case.models import CaseList, CaseClassify
from form_manage.models import FormsManage
from news.models import InfoClassify, InfoList
from system.models import SiteConfiguration, User
from rest_framework import status, mixins
from rest_framework.response import Response
from rest_framework.views import APIView

# 分页器
from utils.pagination import MyPagination, MySinglePagination, MyNewsInfoPagenation, MyCasePagination, NewsINfoPagination
# from utils.MyTimer import my_timer


# 序列化器
from .serializers import ManageImgSerializer, WashCarSerializer, AdvertiseSerializer, TechDevpSerializer, \
    CompanyIntroduceSerializer, CompanyServiceSerializer, CaseListSerializer, InfoListSerializer, PartnerSerializer, \
    DemandTableSerializer, CostAccessSerializer, InfoListSerializer, PartnerTableSerializer, PartnerInfoListSerializer,\
    BottomSerializer, CompanyInfoSerializer, ImgWordsSerializer, SingleNewsDetailSerializer, NewsInfoSerializer, \
    SingleCaseDetailSerializer, CooperateCaseSerializer, AboutQianQianSerializer, WashCarVideoSerializer, \
    ScenarioSerializer


# """首页轮播图展示"""
class ManageImgView(APIView):
    def get(self, request, *args, **kwargs):
        # all_ret = {}
        manage_img_queryset = ManageImg.objects.all().order_by('-index')[:3]
        manage_img_ret = ManageImgSerializer(manage_img_queryset, many=True)
        return Response(manage_img_ret.data, status=status.HTTP_200_OK)


# """首页之我们的服务"""
class IndexServiceView(APIView):
    def get(self, request, *args, **kwargs):
        services = {}
        services2 = {}
        introduce_queryset1 = CompanyService.objects.filter(title='我们的服务').first()
        introduce_queryset2 = CompanyService.objects.filter(position=0,).order_by('index')[1:4]
        # introduce_ret1 = CompanyServiceSerializer(introduce_queryset1, )
        introduce_ret2 = CompanyServiceSerializer(introduce_queryset2, many=True)
        # services['about'] = introduce_ret1.data
        services['title'] = introduce_queryset1.title
        services['about'] = introduce_queryset1.title_desc
        services['content'] = introduce_ret2.data
        return Response(services, status=status.HTTP_200_OK)


# """首页之关于钳钳,钳钳的简单介绍"""
class AboutUsView(APIView):
    def get(self, request, *args, **kwargs):
        # services = {}
        introduce_queryset = CompanyService.objects.filter(position=0, title_level=2, title='关于钳钳').first()
        introduce_ret = AboutQianQianSerializer(introduce_queryset)
        return Response(introduce_ret.data, status=status.HTTP_200_OK)


# """手机移动版 首页"""
class MobileIndexView(APIView):
    def get(self, request, *args, **kwargs):
        mobile = {}
        mobile_img_queryset = ImgWords.objects.filter(belong_page=5).first()  # , img_title='移动版左侧图'

        mobile_icon_queryset = ImgWords.objects.filter(belong_page=5).order_by('index')[1:5]
        mobile_icon_ret = ImgWordsSerializer(mobile_icon_queryset, many=True)

        mobile['left_img'] = IP_PORT + str(mobile_img_queryset.icon)
        mobile['right'] = mobile_icon_ret.data
        return Response(mobile, status=status.HTTP_200_OK)


# """首页的合作案例6个"""
class CaseListView(APIView):
    def get(self, request, *args, **kwargs):
        case_queryset = CaseList.objects.filter(recommend=True).order_by('case_index')[:6]
        case_ret = CaseListSerializer(case_queryset, many=True)
        return Response(case_ret.data, status=status.HTTP_200_OK)


# """首页的新闻资讯4条新闻   """
class IndexNewsView(APIView):
    def get(self, request, *args, **kwargs):
        data = {}
        data['title'] = '新闻资讯'
        data['title_desc'] = '掌握更多行业资讯相关新闻'
        index_news_queryset = InfoList.objects.all().order_by('-commit_time')[:4]    # 最新的4条资讯
        index_news_ret = InfoListSerializer(index_news_queryset, many=True)
        return Response(index_news_ret.data, status=status.HTTP_200_OK)


# """底部导航栏"""
class BottomView(APIView):
    def get(self, request, *args, **kwargs):
        bottom = {}
        bottom_types = BottomCategory.objects.filter(case_category__isnull=True).order_by('index').values_list('classify_name', 'id')
        for type, id in bottom_types:
            bottom_name = BottomCategory.objects.filter(case_category_id=id).order_by('index').values('classify_name', 'url')
            bottom['k%s_name' % id] = type
            bottom['k%s' %id] = list(bottom_name)

        right = SiteConfiguration.objects.values('applet_img', 'customer_service', 'phone', 'address', 'copyright', 'company_name').get()  # 'company_name'
        right['applet_img'] = 'http://' + request.get_host() + '/media/' + right['applet_img']
        right['customer_service'] = 'http://' + request.get_host() + '/media/' + right['customer_service']
        # right['company_name1'] = '广东钳钳科技有限公司'

        bottom['right'] = right
        return Response(bottom, status=status.HTTP_200_OK)


# """业务介绍  我们的服务 """
class IntroduceServiceView(APIView):
    def get(self, request, *args, **kwargs):
        service_queryset = CompanyService.objects.filter(title='我们的服务', position=1).first()
        service_ret = CompanyServiceSerializer(service_queryset)
        # wash['wash_car'] = wash_car_ret.data
        # wash['img_words'] = img_words_ret.data
        return Response(service_ret.data, status=status.HTTP_200_OK)


# """业务介绍  我们的服务 钳钳洗车(有视频的"""
class WashCarView(APIView):
    def get(self, request, *args, **kwargs):
        wash = {}
        wash_car_queryset = WashCar.objects.first()
        wash_car_ret = WashCarSerializer(wash_car_queryset)

        img_words_queryset = ImgWords.objects.filter(belong_page=1, belong_to=1).order_by('index')
        img_words_ret = ImgWordsSerializer(img_words_queryset, many=True)

        background_image = ImgWords.objects.filter(img_title='04_关于钳钳_top', belong_page=6).first().icon
        background_image_url = 'http://' + request.get_host() + '/media/' + str(background_image)

        video_queryset = WashCar.objects.first()
        video_ret = WashCarVideoSerializer(wash_car_queryset)

        wash['video_part'] = video_ret.data
        wash['background_image'] = background_image_url
        wash['wash_car'] = wash_car_ret.data
        wash['img_words'] = img_words_ret.data
        return Response(wash, status=status.HTTP_200_OK)


# """03_partner企业介绍下的合伙人图(很多个)或者 13_partner企业介绍下的合伙人图5个"""
class PartnerView(APIView):
    def get(self, request, *args, **kwargs):
        data = {}

        data['title']= '合作伙伴案例'
        data['title_desc']= '您的选择是对我们的肯定，我们在这里期待与您合作！'

        if kwargs.get('partner') == '03_partner':
            partner_queryset = Partner.objects.order_by('index')
        else:
            partner_queryset = Partner.objects.order_by('index')[:5]

        partner_ret = PartnerSerializer(partner_queryset, many=True)
        data['partner_img'] = partner_ret.data
        return Response(data, status=status.HTTP_200_OK)


# '''小图标和文字介绍'''
class ImgWordsView(APIView):
    def get(self, request, *args, **kwargs):
        # services = {}
        introduce_queryset = ImgWords.objects.filter(position=1,).order_by('index')
        introduce_ret = ImgWordsSerializer(introduce_queryset, many=True)
        return Response(introduce_ret.data, status=status.HTTP_200_OK)


# """公司简介上"""
class CompanyIntroduceView(APIView):
    def get(self, request, *args, **kwargs):
        profile_queryset = CompanyIntroduce.objects.all()
        profile_ret = CompanyIntroduceSerializer(profile_queryset, many=True)

        return Response(profile_ret.data, status=status.HTTP_200_OK)


# """公司简介下的地址"""
class CompanyAddressView(APIView):
    def get(self, request, *args, **kwargs):
        profile_dict = {}
        introduce_right = CompanyIntroduce.objects.values_list('branch_office_01', 'branch_office_02',
                                                               'branch_office_03', 'branch_office_04').get()
        introduce_right = ['分部：' + i for i in introduce_right]

        introduce_left = CompanyIntroduce.objects.values_list('company_name', 'telephone', 'WeChat', 'headquarters').get()
        icon_title = ImgWords.objects.filter(belong_page=3).values('icon', 'img_title').order_by('index')
        count = 0
        for i in icon_title:
            i['icon'] = "http://" + request.get_host() + "/media/" + str(i['icon'])
            if count > 0:
                i['img_title'] = i['img_title'] + ":  " + introduce_left[count]
            count += 1
        profile_queryset = CompanyIntroduce.objects.all()
        profile_ret = CompanyIntroduceSerializer(profile_queryset, many=True)
        profile_dict['left'] = icon_title
        profile_dict['right'] = introduce_right
        return Response(profile_dict, status=status.HTTP_200_OK)


# """技术开发"""
class TechDevpView(APIView):
    def get(self, request, *args, **kwargs):
        technology = {}
        technology_queryset = CompanyService.objects.filter(title='技术开发', position=1).first()
        technology_ret = CompanyServiceSerializer(technology_queryset)

        img_words_queryset = ImgWords.objects.filter(belong_page=1, belong_to=2).order_by('index')
        img_words_ret = ImgWordsSerializer(img_words_queryset, many=True)
        technology['technology_ret'] = technology_ret.data
        technology['img_words'] = img_words_ret.data
        return Response(technology, status=status.HTTP_200_OK)


# """04 关于钳钳 之钳钳理念"""
class TheoryView(APIView):
    def get(self, request, *args, **kwargs):
        theory_queryset = CompanyService.objects.filter(title='钳钳理念').first()
        theory_ret = CompanyServiceSerializer(theory_queryset)
        number1 = {
            'number': 100,
            'desc': '上线天数'
            }
        number2 = {
            'number': 10000,
            'desc': '入驻商家'
        }
        number3 = {
            'number': 50000,
            'desc': '服务用户'
        }
        number4 = {
            'number': 1000,
            'desc': '全国县市'
        }

        # content = [number1, number2, number3, number4]
        # content = QianQianNumber.objects.filter(id=1).values('begin_day', 'interval_days', 'begin_number').get()
        content = QianQianNumber.objects.values('title', 'now_number')  # filter(id=1).  .get()

        number = {}
        number.update(theory_ret.data)
        number['content'] = content
        return Response(number, status=status.HTTP_200_OK)


# """04页关于钳钳 之合伙人计划 合伙人计划提交表,表在system/表单管理里面"""
class PartnerTableView(APIView):
    def get(self, request, *args, **kwargs):
        partner = {}
        partner_queryset_01 = SiteConfiguration.objects.values("WeChat_img", "customer_service").get()
        contract = CompanyIntroduce.objects.values("telephone", "WeChat").get()
        all_dress = CompanyIntroduce.objects.all().values_list("headquarters", "branch_office_01", "branch_office_02",
                                                               "branch_office_03", "branch_office_04").get()
        WeiChat_image = {}
        for i, j in partner_queryset_01.items():
            j = "http://" + request.get_host() + "/media/" + str(j)
            WeiChat_image[i] = j
        partner.update(WeiChat_image)
        partner.update(contract)
        partner["address"] = all_dress
        return Response(partner, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = PartnerTableSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()  # 如果验证通过,保存到数据库中
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  # 错误信息包含在errors里面



class CaseBackgroundView(APIView):
    def get(self, request, *args, **kwargs):
        data = {}
        background_image = ImgWords.objects.filter(img_title='合作案例_全部案例_top', belong_page=6).first().icon
        background_image_url = "%s%s" % (IP_PORT, background_image)

        data['background_image'] = background_image_url
        return Response(data, status=status.HTTP_200_OK)


class BackgroundRetrieveView(APIView):
    def get(self, request, id, *args, **kwargs):
        data = {}
        background_image = ImgWords.objects.filter(id=id, belong_page=6).first()
        data['background_image'] = 'http://' + request.get_host() + '/media/' + str(background_image.icon)
        return Response(data, status=status.HTTP_200_OK)


class ServiceIndustryView(APIView):
    def get(self, request, *args, **kwargs):
        data = {}
        service = [i for i in CaseClassify.objects.filter(industry_choices=1).values('types', 'id')]
        industry = [i for i in CaseClassify.objects.filter(industry_choices=2).values('types', 'id')]

        background_image = ImgWords.objects.filter(img_title='合作案例_全部案例_top', belong_page=6).first().icon
        background_image_url = "%s%s" % (IP_PORT, background_image)

        data['background_image'] = background_image_url
        data['service'] = service
        data['industry'] = industry
        return Response(data, status=status.HTTP_200_OK)


# """合作案例页面下的默认全部合作案例分页每页9条"""
class CooperateCasePageView(APIView,):
    def get(self, request, *args, **kwargs):
        service = request.query_params.get('service', None)
        industry = request.query_params.get('industry', None)

        # offset = request.query_params.get('offset', 0)
        # limit = request.query_params.get('limit', 9)

        # id = request.query_params.get('id', None)
        # all_case = list(CaseList.objects.all().order_by('-case_index'))
        if not service and not industry:
            case_queryset = CaseList.objects.all().order_by('-case_index')

        elif service and not industry and service.isdigit():
        # elif service and not industry :
            case_queryset = CaseList.objects.filter(service_type=service).order_by('-case_index')

        elif not service and industry and industry.isdigit():
            case_queryset = CaseList.objects.filter(industry_classify=industry).order_by('-case_index')

        elif service and industry and industry.isdigit() and service.isdigit():
            case_queryset = CaseList.objects.filter(service_type=service, industry_classify=industry).\
                order_by('-case_index')
        else:
            return Response({"status_code": 400, "message": "service和industry携带参数必须都是正整数"},
                            status=status.HTTP_400_BAD_REQUEST)
        # data = {}
        # case_list = case_queryset.data
        # paginator = Paginator(case_queryset, 9)

        page_obj = MyCasePagination()
        page_case = page_obj.paginate_queryset(queryset=case_queryset, request=request, view=self)
        ret = CooperateCaseSerializer(page_case, many=True)
        # data['detail'] = ret.data  # 数据总数
        return page_obj.get_paginated_response(ret.data)


class DemandTableView(mixins.CreateModelMixin, GenericViewSet):
    serializer_class = DemandTableSerializer

    def create(self, request, *args, **kwargs):
        # print('--------------', request.data, "+++++++++++++++++++++")
        # print('类型', type(request.data))
        # print(request.data.get("content").split(','),'撒旦法师打发')
        # print(dict(request.data.get("content")),'士大夫')

        content = request.data.get("content")
        # print('----------',content , '-----------+')
        # content1= ""
        # content2= ""
        # for i in request.data.get("content").split(','):
        # for i in request.data.get("content"):
        #     print('00++++++++++', i,'01+++++++++++++++')
        #     content1 += "%s \r\n " %i
        #     # content2 += "%s   ---------  ;  " %i
        #     content2 += "%s   ;  " %i
        # print('11###############', content1 ,'22##########')
        #
        # content =content1

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        company = serializer.validated_data.get("company")
        type = serializer.validated_data.get("type")
        name = serializer.validated_data.get("name")
        telephone = serializer.validated_data.get("telephone")
        email = serializer.validated_data.get("email")
        # content = serializer.validated_data.get("content")
        # for i in serializer.validated_data.get("content"):
        #     print('++++++++++', i,'+++++++++++++++')
        #     contentt =  i + '  \r\n'

        # FormsManage.objects.create(type=type, company=company, name=name, telephone=telephone, email=email,content =content2, content1 =content2)
        FormsManage.objects.create(type=type, company=company, name=name, telephone=telephone, email=email,content =content )
        return Response({"传入数据成功": request.data}, status=status.HTTP_200_OK)


# """合作案例里面的需求意向表,表在FormsManage表单管理里面,只是获取表的提交,get方法无用"""
# class DemandTableView(GenericViewSet):
# class DemandTableView(APIView):
# class DemandTableView(mixins.CreateModelMixin, GenericViewSet):
#     def get(self, request, *args, **kwargs):
#         return Response('是打发第三方')
#     def list(self, request, *args, **kwargs):
#         return Response('是打发第三方')
    # def post(self, request, *args, **kwargs):
    #     serializer = DemandTableSerializer(data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()  # 如果验证通过,保存到数据库中
    #         return Response(serializer.data)
    #     else:
    #         return Response(serializer.errors)  # 错误信息包含在errors里面


# """合作案例里面的费用评估表,表在FormsManage表单管理里面,只是获取表的提交,get方法无用"""
class CostAccessTableView(APIView):
    def post(self, request, *args, **kwargs):
        serializer = CostAccessSerializer(data=request.data)
        # serializer = DemandTableSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()  # 如果验证通过,保存到数据库中
            return Response(serializer.data)
        else:
            return Response(serializer.errors)  # 错误信息包含在errors里面


# """07单条具体的案例详情页面单独的资讯id"""
class CaseDetailView(APIView):
    def get(self, request, id):
        detail = {}
        single_case_queryset = CaseList.objects.filter(id=id).first()
        # case_name = CaseList.objects.filter(id=id).first().case_name
        single_case_ret = SingleCaseDetailSerializer(single_case_queryset)
        # detail['case_name'] = CaseList.objects.filter(id=id).first().case_name
        pc_image_01 = CaseList.objects.filter(id=id).values_list('pc_img_01', 'pc_img_02', 'pc_img_03', 'pc_img_04').get()
        mobile_image_01 = CaseList.objects.filter(id=id).values_list('mobile_img_01', 'mobile_img_02', 'mobile_img_03',
                                                                     'mobile_img_04').get()
        pc_image = ['http://' + request.get_host() + '/media/' + k for k in pc_image_01 if len(k) > 0]
        mobile_image = ['http://' + request.get_host() + '/media/' + k for k in mobile_image_01 if len(k) > 0]

        detail['pc_image'] = pc_image
        detail['mobile_image'] = mobile_image
        detail.update(single_case_ret.data)
        # detail['introduce'] = single_case_ret.data

        return Response(detail, status=status.HTTP_200_OK)


# """资讯页面公告分类和行业新闻"""  需要携带参数?category=1或2默认 category=2 公告 1是行业新闻
class NewsInfoView(APIView):
    def get(self, request, *args, **kwargs):
        try:
            category = int(request.query_params.get('category', 2))
        except Exception as e:
            return Response({"status_code":400, "message":"category携带参数必须是1或者2"}, status=status.HTTP_400_BAD_REQUEST)

        info_queryset = InfoList.objects.filter(category=category).order_by('-commit_time')
        # 1, 实例化分页器对象  MyNewsInfoPagenation MyINfoNewsPagination
        # page_obj = MyNewsInfoPagenation()
        page_obj = NewsINfoPagination()  # offsetLimitOffset   分页

        # 2，调用分页方法去分页queryset
        page_queryset = page_obj.paginate_queryset(info_queryset, request, view=self)
        # 3，把分页好的数据序列化返回
        # 4, 带着上一页下一页连接的响应
        ser_obj = NewsInfoSerializer(page_queryset, many=True)
        return page_obj.get_paginated_response(ser_obj.data)


# """单条具体的资讯详情页面单独的资讯id"""
class SingleNewsDetailView(APIView):
    def get(self, request, id):
        single_news_queryset = InfoList.objects.filter(id=id).first()
        single_news_ret = SingleNewsDetailSerializer(single_news_queryset)
        return Response(single_news_ret.data, status=status.HTTP_200_OK)


# """11页,单条具体的资讯详情页面上下篇"""
class PreviousNextView(APIView):
    def get(self, request, *args, **kwargs):
        if kwargs.get('classification') == 'info_notice':
            category = 2
        elif kwargs.get('classification') == 'info_news':
            category = 1
        info_queryset = InfoList.objects.filter(category=category).all()
        # 1, 实例化分页器对象
        page_obj = MySinglePagination()
        # 2，调用分页方法去分页queryset
        page_queryset = page_obj.paginate_queryset(info_queryset, request, view=self)
        # 3，把分页好的数据序列化返回
        # 4, 带着上一页下一页连接的响应
        ser_obj = NewsInfoSerializer(page_queryset, many=True)
        return page_obj.get_paginated_response(ser_obj.data)


# """资讯右侧的推荐"""
class NewsRecommendView(APIView):
    def get(self, request, *args, **kwargs):
        info_queryset = InfoList.objects.all().order_by('-recommend_index')[:6]   # 'clicks', 'commit_time'
        info_obj = InfoListSerializer(info_queryset, many=True)
        return Response(info_obj.data, status=status.HTTP_200_OK)


# '''业务介绍页面的钳钳洗车和技术开发的小图标和文字,需要携带参数1或者2'''
class ImgWordsView(APIView):
    def get(self, request, *args, **kwargs):
        try:
            belong = int(request.query_params.get('belong', 1))
        except Exception as e:
            return Response({"status_code":400, "message":"belong携带参数必须是1或者2"}, status=status.HTTP_400_BAD_REQUEST)

        img_words_queryset = ImgWords.objects.filter(belong_to=belong).order_by('index')

        img_words_obj = ImgWordsSerializer(img_words_queryset, many=True)
        return Response(img_words_obj.data, status=status.HTTP_200_OK)


# """业务介绍下的之广告设计"""
class IntroduceAdvertiseView(APIView):
    def get(self, request, *args, **kwargs):
        advertise_queryset = CompanyService.objects.filter(position=1, title='广告设计').first()
        advertise_ret = CompanyServiceSerializer(advertise_queryset)
        return Response(advertise_ret.data, status=status.HTTP_200_OK)


# """05 技术开发服务页下的技术开发"""
class ServiceTechnologyView(APIView):
    def get(self, request, *args, **kwargs):
        service_dict = {}
        technology_icon01 = ImgWords.objects.filter(belong_page=2, belong_to=2).order_by('index')
        technology_icon02 = ImgWords.objects.filter(belong_page=2, belong_to=3).order_by('index')[1:]
        technology_back = ImgWords.objects.filter(img_title='流程').first()
        technology_icon01_ret = ImgWordsSerializer(technology_icon01, many=True)
        technology_icon02_ret = ImgWordsSerializer(technology_icon02, many=True)
        technology_back_ret = ImgWordsSerializer(technology_back)

        background_image = ImgWords.objects.filter(img_title='05_技术开发_top', belong_page=6).first().icon
        background_image_url = "%s%s" % (IP_PORT, background_image)

        service_dict['background_image'] = background_image_url
        service_dict['up'] = technology_icon01_ret.data
        service_dict['down'] = technology_icon02_ret.data
        service_dict['middle'] = technology_back_ret.data
        return Response(service_dict, status=status.HTTP_200_OK)


# """05 技术开发服务页下的应用场景"""
class ScenarioView(APIView):
    def get(self, request, *args, **kwargs):
        scenario_queryset = ImgWords.objects.filter(belong_page=7).order_by('index')
        # print('阿顺丰到付',scenario_queryset)
        scenario_ret = ScenarioSerializer(scenario_queryset, many=True)
        # print('阿顺丰到付范德萨', scenario_ret)
        return Response(scenario_ret.data, status=status.HTTP_200_OK)


# ""广告设计服务页之6个广告设计展示"""
class AdvertiseView(APIView):
    def get(self, request, *args, **kwargs):
        data = {}
        advertise_queryset = Advertise.objects.all().order_by('index')[:6]
        advertise_ret = AdvertiseSerializer(advertise_queryset, many=True)
        # title_background = ImgWords.objects.filter(img_title='广告设计', belong_page=6).values('img_title', 'icon', 'services').get()
        title_background = ImgWords.objects.filter(img_title='广告设计', belong_page=6).values('img_title', 'services').get()
        # print(title_background)

        # background_image = ImgWords.objects.filter(img_title='04_关于钳钳_top', belong_page=6).first().icon
        # title_background['icon'] = 'http://' + request.get_host() + '/media/' + title_background['icon']
        # print(title_background['background'],'成都市发放')

        data.update(title_background)
        data['bottom'] = advertise_ret.data

        return Response(data, status=status.HTTP_200_OK)


# ""广告设计服务页之流程"""
class AdProcessView(APIView):
    def get(self, request, *args, **kwargs):
        process = {}
        process_background = ImgWords.objects.filter(belong_page=4, img_title='流程').first()
        process_background_ret = ImgWordsSerializer(process_background)

        process_queryset = ImgWords.objects.filter(belong_page=4).order_by('index')[1:]     #
        process_ret = ImgWordsSerializer(process_queryset, many=True)
        process['background'] = process_background_ret.data
        process['detail'] = process_ret.data
        return Response(process, status=status.HTTP_200_OK)


# """广告设计服务页之流程"""
class TimerView(APIView):
    def get(self, request, *args, **kwargs):
        # a = QianQianNumber.objects.filter(id=1).values('begin_day', 'interval_days', 'begin_number').get()
        # b = MyTimer(a['begin_day'], a['interval_days'], a['begin_number'])
        # b = b.my_timer()

        number = QianQianNumber.objects.filter(id=1).values('begin_day', 'interval_days', 'begin_number').get()
        today = datetime.date.today()

        # while True:
            # time.sleep(number['interval_days']*24*3600)
        time.sleep(3)
        print(number['begin_day'],number['begin_number'],type(number['begin_number']))

        sum_number = (today - number['begin_day']).days + int(number['begin_number'])
        print(sum_number, '发电房', type(sum_number))
        obj = QianQianNumber.objects.filter(id=1).update(now_number=sum_number)
            # add_number = (today - number['begin_day']).days
            # sum_number = add_number + self.begin_number
            # return '修改成功'

        return Response('ok1')

# """钳钳理念动态的上线天数变化"""
def my_timer(id):
    number = QianQianNumber.objects.filter(id=1).values('begin_day', 'interval_days', 'begin_number').get()
    today = datetime.date.today()
    while True:
        # time.sleep(number['interval_days']*24*3600)
        time.sleep(300)
        # sum_number = (today - number['begin_day']).days + int(number['begin_number'])
        sum_number = 1 + int(number['begin_number'])
        # print(sum_number, '发电房', type(sum_number))
        obj = QianQianNumber.objects.filter(id=1).update(now_number=sum_number)


def main():
    print("main thread")
    # t = ['t'+ str(i) for i in range(4)]
    # for index, value in enumerate(t):
    #     print(value, '撒旦法02')
    #     value = threading.Thread(target=my_timer, args=(index+1))
    #     print(value,'撒旦法')
    #     value.setDaemon(True)
    #     value.start()
    # for i  in  range(4):
    t1 = threading.Thread(target=my_timer, args=('1',))
    # t1= threading.Thread(target=my_timer,args=(1))
    # t2 = threading.Thread(target=my_timer,args=(2))
    # t3 = threading.Thread(target=my_timer,args=(3))
    # t4 = threading.Thread(target=my_timer,args=(4))
    t1.setDaemon(True)
    t1.start()
    # time.sleep(1)
    print("main thread end")

main()