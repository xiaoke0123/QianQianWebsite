from django.apps import AppConfig


class PageManageConfig(AppConfig):
    name = 'apps.page_manage'
    verbose_name = '页面管理'

