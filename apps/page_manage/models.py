# -*- encoding: utf-8 -*-
from django.contrib.auth.models import AbstractUser, Group
from django.db import models

from stdimage.models import StdImageField
from stdimage.utils import UploadToUUID, UploadToClassNameDirUUID


# """页面管理的模型抽象基类使所有模型类继承这些字段"""
class PageManageBaseModel(models.Model):
    title = models.CharField(max_length=40, verbose_name='标题')
    # keywords = models.CharField(max_length=40, verbose_name='关键词', null=True, blank=True)
    title_desc = models.CharField(max_length=300, null=True, blank=True, verbose_name='描述')
    # 设置后台显示的富文本编辑器的宽高，以及图片，文件保存目录

    class Meta:
        # 说明是一个抽象模型类
        abstract = True

    def __str__(self):
        return self.title


# """页面管理之业务介绍"""
class BusinessIntroduce(PageManageBaseModel):
    class Meta:
        # db_table = 'PG_BusinessIntroduce'
        db_table = 'pg_businessIntroduce'
        verbose_name = '业务介绍'
        verbose_name_plural = verbose_name


# """页面管理之钳钳洗车"""
class WashCar(models.Model):
    title = models.CharField(max_length=40, verbose_name='标题')
    en_name = models.CharField(max_length=40, verbose_name='关键词', null=True, blank=True, default='QianQian Car Wash')
    title_desc = models.CharField(max_length=300, null=True, blank=True, verbose_name='描述')
    video = models.FileField(verbose_name='洗车视频上传')
    service_img = StdImageField(max_length=100, verbose_name=u"服务简介图",
                                upload_to=UploadToUUID(path='page_manage/images'),
                                variations={'thumbnail': {'width': 100, 'height': 75}},
                                null=True, blank=True, default=0)
    service_img2 = StdImageField(max_length=100, verbose_name=u"服务简介图",
                                 upload_to=UploadToUUID(path='page_manage/images'),
                                 variations={'thumbnail': {'width': 100, 'height': 75}},
                                 null=True, blank=True, default=0)

    def image_service_img(self):
        if self.service_img:
            return str('<img src="%s" />' % self.service_img.thumbnail.url)
        else:
            return u'上传图片'
    image_service_img.short_description = '洗车简介图01'
    image_service_img.allow_tags = True

    def image_service_img2(self):
        if self.service_img:
            return str('<img src="%s" />' % self.service_img2.thumbnail.url)
        else:
            return u'上传图片'
    image_service_img2.short_description = '洗车简介图2'
    image_service_img2.allow_tags = True

    class Meta:
        db_table = 'pg_washcar'
        verbose_name = '钳钳洗车'
        verbose_name_plural = verbose_name


# """页面管理业务介绍 之技术开发"""
class TechDevp(PageManageBaseModel):
    service_img = StdImageField(max_length=100, verbose_name=u"服务简介图",
                                upload_to=UploadToUUID(path='page_manage/images'),
                                variations={'thumbnail': {'width': 100, 'height': 75}},
                                null=True, blank=True, default=0)

    # upload_to=UploadToUUID(path=datetime.now().strftime('banner/%Y/%m')),

    def image_service_img(self):
        if self.ServiceImg:
            return str('<img src="%s" />' % self.image_service_img.thumbnail.url)
        else:
            return u'上传图片'
    image_service_img.short_description = '服务简介图'
    image_service_img.allow_tags = True

    class Meta:
        # db_table = 'PG_TechDevp'
        db_table = 'pg_techdevp'
        verbose_name = '技术开发'
        verbose_name_plural = verbose_name


# """11页 之广告设计展示"""
class Advertise(models.Model):
    title = models.CharField(max_length=40, verbose_name='标题')
    index = models.SmallIntegerField(default=0, verbose_name='展示顺序', null=True, blank=True)
    service_img = StdImageField(max_length=100, verbose_name=u"服务简介图",
                                upload_to=UploadToUUID(path='page_manage/images'),
                                # upload_to=UploadToClassNameDirUUID(),
                                variations={'thumbnail': {'width': 100, 'height': 75}},
                                null=True, blank=True, default=0)

    def image_service_img(self):
        if self.service_img:
            return str('<img src="%s" />' % self.service_img.thumbnail.url)
        else:
            return u'上传图片'
    image_service_img.short_description = '展示图'
    image_service_img.allow_tags = True

    class Meta:
        # db_table = 'PG_Advertise'
        db_table = 'pg_advertise'
        verbose_name = '广告设计'
        verbose_name_plural = verbose_name


# """公司信息表"""
class CompanyInfo(models.Model):
    company_name = models.CharField(max_length=20, null=True, blank=True)
    head_or_branch = models.SmallIntegerField(choices=((0, '分部'), (1, '总部')), verbose_name='总部或分部', default=0)
    telephone = models.CharField(verbose_name='电话', max_length=12, null=True, blank=True)
    WeiChat = models.CharField(verbose_name='微信', max_length=25, null=True, blank=True)
    address = models.CharField(verbose_name='公司地址', max_length=40)
    index = models.SmallIntegerField(default=0, verbose_name='展示顺序', null=True, blank=True)

    class Meta:
        # db_table = 'CompanyInfo'
        db_table = 'companyinfo'
        verbose_name = '公司信息'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.company_name


# """新的页面管理的模型类"""
class NewPageManageBaseModel(models.Model):
    title = models.CharField(max_length=40, verbose_name='标题')
    title_desc = models.TextField(max_length=80, verbose_name='标题简述', null=True, blank=True)
    en_name = models.CharField(max_length=20, null=True, blank=True, verbose_name='英文名')
    desc = models.TextField(max_length=300, null=True, blank=True, verbose_name='描述')
    image = models.ImageField(upload_to='type', verbose_name='标题图片', null=True, blank=True)
    index = models.SmallIntegerField(verbose_name='页面顺序', null=True, blank=True)

    class Meta:
        # db_table = 'NewPageManage'
        db_table = 'newpagemanage'
        verbose_name = '页面管理'
        verbose_name_plural = verbose_name
        abstract = True

    def __str__(self):
        return self.title


# """公司简介模型类"""
class CompanyIntroduce(models.Model):
    background_image = models.ImageField(upload_to='type', verbose_name='上层背景图', null=True, blank=True)
    title = models.CharField(max_length=40, verbose_name='标题')
    title_desc = models.TextField(max_length=80, verbose_name='标题简述', null=True, blank=True)
    # en_name = models.CharField(max_length=20, null=True, blank=True, verbose_name='英文名')
    desc = models.TextField(max_length=300, null=True, blank=True, verbose_name='描述')
    image = models.ImageField(upload_to='type', verbose_name='标题图片', null=True, blank=True)
    # index = models.SmallIntegerField(verbose_name='页面顺序', null=True, blank=True)
    contact = models.TextField(verbose_name='公司地址及联系电话', default='电话')
    company_name = models.CharField(verbose_name='公司名', max_length=20)

    telephone = models.CharField(max_length=40, verbose_name='电话')
    WeChat = models.CharField(max_length=40, verbose_name='微信')

    headquarters = models.CharField(max_length=40, verbose_name='总公司地址')
    branch_office_01 = models.CharField(max_length=40, verbose_name='分公司地址_01')
    branch_office_02 = models.CharField(max_length=40, verbose_name='分公司地址_02')
    branch_office_03 = models.CharField(max_length=40, verbose_name='分公司地址_03')
    branch_office_04 = models.CharField(max_length=40, verbose_name='分公司地址_04')

    class Meta:
        # db_table = 'CompanyIntroduce'
        db_table = 'companyintroduce'
        verbose_name = '公司简介'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title


# """图标文字模型类"""
class ImgWords(models.Model):
    icon = StdImageField(max_length=100, verbose_name="小图标",
                         upload_to=UploadToUUID(path='page_manage/icon'),
                         variations={'thumbnail': {'width': 45, 'height': 60}},
                         null=True, blank=True, default=0)

    background_image = StdImageField(max_length=100, verbose_name="背景图",
                                     upload_to=UploadToUUID(path='page_manage/background_image'),
                                     variations={'thumbnail': {'width': 45, 'height': 60}},
                                     null=True, blank=True, default=0)

    img_title = models.CharField(max_length=20, verbose_name='图片标题', null=True, blank=True)
    services = models.TextField(verbose_name='服务类型', null=True, blank=True)
    index = models.SmallIntegerField(verbose_name='顺序', null=True, blank=True, default=0)
    belong_page = models.SmallIntegerField(verbose_name='所属页面', choices=((1, '02业务介绍页面'), (2, '05技术开发服务页面'),
                                                                          (3, '企业介绍_公司简介'), (4, '13广告设计服务页_流程'), (5, '手机版首页'),
                                                                          (6, '背景图'), (7, '05技术开发_应用场景')), null=True, blank=True)  # , (8, '05技术开发服务页面_流程') (2, '05技术开发服务页面_技术开发')
    belong_to = models.SmallIntegerField(verbose_name='所属标题', choices=((1, '钳钳洗车'), (2, '技术开发'), (3, '流程'), (5, '合作伙伴')), null=True, blank=True,)

    def image_icon(self):
        if self.icon:
            return str('<img src="%s" />' % self.icon.thumbnail.url)
        else:
            return '上传图片'

    image_icon.short_description = '小图标'
    image_icon.allow_tags = True

    class Meta:
        db_table = 'imgwords'
        verbose_name = '图标,文字,背景图'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.img_title


# """业务介绍之我们的服务模型类"""
class CompanyService(models.Model):
    title = models.CharField(max_length=40, verbose_name='标题')
    title_desc = models.TextField(max_length=280, verbose_name='标题简述', null=True, blank=True)
    en_name = models.CharField(max_length=20, null=True, blank=True, verbose_name='英文名')
    desc = models.TextField(max_length=300, null=True, blank=True, verbose_name='描述')
    image = models.ImageField(upload_to='type', verbose_name='标题图片', null=True, blank=True)
    index = models.SmallIntegerField(verbose_name='页面顺序', null=True, blank=True)

    title_level = models.SmallIntegerField(choices=((1, '一级'), (2, '二级'), (3, '三级')),
                                           verbose_name='展示等级', default=2, null=True, blank=True,)
    service_index = models.SmallIntegerField(verbose_name='我们的服务下的顺序', null=True, blank=True, default=0)
    position = models.SmallIntegerField(choices=((0, '首页'), (1, '业务介绍'), (3, '关于钳钳')), verbose_name='所属页面', default=0)

    class Meta:
        # db_table = 'CompanyService'
        db_table = 'companyservice'
        verbose_name = '我们的服务'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title


# # """关于钳钳之钳钳理念 数量展示 """
class QianQianNumber(models.Model):
    title = models.CharField(max_length=40, verbose_name='名称', null=True, blank=True)
    begin_day = models.DateField(auto_now_add=True, verbose_name='上线开始时间', null=True, blank=True)
    begin_number = models.PositiveSmallIntegerField(verbose_name='开始数量')
    interval_days = models.PositiveSmallIntegerField(verbose_name='可变化间隔天数')
    now_number = models.PositiveIntegerField(verbose_name='现在数量')

    class Meta:
        # db_table = 'QianQianNumber'
        db_table = 'qianqiannumber'
        verbose_name = '钳钳理念的展示数据'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title



