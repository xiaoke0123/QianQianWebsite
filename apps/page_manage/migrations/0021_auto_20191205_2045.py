# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2019-12-05 20:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('page_manage', '0020_auto_20191204_1755'),
    ]

    operations = [
        migrations.CreateModel(
            name='QianQianNumber',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=40, null=True, verbose_name='入驻商家数量')),
                ('begin_day', models.DateField(auto_now_add=True, null=True, verbose_name='上线开始时间')),
                ('begin_number', models.PositiveSmallIntegerField(verbose_name='开始数量')),
                ('now_number', models.PositiveIntegerField(verbose_name='现在数量')),
            ],
            options={
                'verbose_name': '钳钳理念的展示数据',
                'verbose_name_plural': '钳钳理念的展示数据',
                'db_table': 'QianQianNumber',
            },
        ),
        migrations.AlterField(
            model_name='imgwords',
            name='belong_page',
            field=models.SmallIntegerField(blank=True, choices=[(1, '02业务介绍页面'), (2, '05技术开发服务页面'), (3, '企业介绍_公司简介'), (4, '13广告设计服务页_流程'), (5, '手机版首页'), (6, '背景图'), (7, '05技术开发_应用场景')], null=True, verbose_name='所属页面'),
        ),
    ]
