# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2019-12-03 18:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('page_manage', '0016_auto_20191202_1828'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='imgwords',
            options={'verbose_name': '图标,文字,背景图', 'verbose_name_plural': '图标,文字,背景图'},
        ),
        migrations.AddField(
            model_name='companyintroduce',
            name='branch_office_01',
            field=models.CharField(default='fdsa', max_length=40, verbose_name='分公司地址_01'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='companyintroduce',
            name='branch_office_02',
            field=models.CharField(default='sdaf', max_length=40, verbose_name='分公司地址_02'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='companyintroduce',
            name='branch_office_03',
            field=models.CharField(default='sdf', max_length=40, verbose_name='分公司地址_03'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='companyintroduce',
            name='branch_office_04',
            field=models.CharField(default='sdafs', max_length=40, verbose_name='分公司地址_04'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='companyintroduce',
            name='headquarters',
            field=models.CharField(default='sdfs', max_length=40, verbose_name='总公司地址'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='imgwords',
            name='belong_page',
            field=models.SmallIntegerField(blank=True, choices=[(1, '02业务介绍页面'), (2, '05技术开发服务页面'), (3, '企业介绍'), (4, '13广告设计服务页'), (5, '手机版首页'), (6, '背景图')], null=True, verbose_name='所属页面'),
        ),
    ]
