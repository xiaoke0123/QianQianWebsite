# from extra_apps import xadmin
# # from extra_apps.xadmin import views
from django.shortcuts import render, redirect, HttpResponse
import xadmin
from xadmin import views

from .models import BusinessIntroduce, WashCar,  TechDevp, CompanyIntroduce, ImgWords, CompanyService, Advertise, QianQianNumber

# Register your models here.


# 自定义的PageManageAdmin页面管理
class PageManageAdmin(object):
    # model_icon = 'fa fa-envelope'
    list_per_page = 30
    show_bookmarks = False  # 关闭书签
    list_display = ['id', 'title', 'keywords', 'desc']
    list_editable = ['user']
    style_fields = {'detailed_content': 'ueditor'}
    # exclude = ['image_service_img', ]  # 不显示字段


# 自定义的PageManageAdmin页面管理之我们的服务
class PageManageServiceAdmin(object):
    # model_icon = 'fa fa-envelope'
    list_per_page = 30
    show_bookmarks = False  # 关闭书签
    list_display = ['id', 'title', 'image_service_img']
    list_editable = ['id', 'title', 'image_service_img']
    style_fields = {'detailed_content': 'ueditor'}
    # readonly_fields = ['title']


# 自定义的PageManageAdmin页面管理之我们的服务
class NewPageManageServiceAdmin(object):
    # model_icon = 'fa fa-envelope'
    list_per_page = 30
    show_bookmarks = False  # 关闭书签
    # inlines = [ImgWords]
    # list_display = ['id', 'title', 'image_service_img', 'keywords', 'desc']
    # list_editable = ['user']
    # style_fields = {'detailed_content': 'ueditor'}
    # readonly_fields = ['title']


# 自定义的我们的服务CompanyServiceAdmin页面管理之
class CompanyServiceAdmin(object):
    # model_icon = 'fa fa-envelope'
    list_per_page = 30
    show_bookmarks = False  # 关闭书签
    list_display = ['title', 'index', 'image', 'title_level', 'service_index', 'position', 'title_desc', 'en_name', 'desc']
    list_editable = ['service_index', 'image', 'title_level', 'title', 'index', 'position', 'title_desc', 'en_name', 'desc']
    ordering = ('position', 'index', 'title_level')
    readonly_fields = ['title', 'position']



class QianQianNumberInline(object):
    model = QianQianNumber
    extra = 0


class QianQianNumberAdmin(object):
    show_bookmarks = False  # 关闭书签
    # inlines = [ImgWords]
    list_display = ['title', 'now_number', 'begin_number', 'begin_day', 'interval_days']
    list_editable = ['title', 'now_number', 'begin_number', 'begin_day', 'interval_days']

# class CourseAdmin(object):
#     list_display = []
#     search_field = []
#     ...
#     inlines = [QianQianNumberInline]

# 自定义的公司简介页面管理
class CompanyIntroduceAdmin(object):
    # model_icon = 'fa fa-envelope'
    list_per_page = 30
    show_bookmarks = False  # 关闭书签
    # inlines = [ImgWords]
    list_display = ['title', 'title_desc', 'desc']
    list_editable = ['title', 'title_desc', 'desc']
    # ordering = ('index',)
    # readonly_fields = ['title']
    # inlines = [QianQianNumberInline]


# 自定义的PageManageAdmin页面管理之我们的服务
class ImgWordsAdmin(object):
    # model_icon = 'fa fa-envelope'
    list_per_page = 30
    show_bookmarks = False  # 关闭书签
    list_display = ['id', 'image_icon', 'img_title', 'belong_page', 'index', 'services', 'belong_to']  # 'icon',
    list_editable = ['icon', 'image_icon', 'img_title', 'belong_page', 'index', 'services', 'belong_to']
    ordering = ('belong_page', 'belong_to', 'index',)
    # style_fields = {'detailed_content': 'ueditor'}
    # readonly_fields = ['id', 'img_title']
    search_fields = ['img_title', 'services', 'belong_page']

    list_filter = ['img_title', 'services', 'belong_page']    #   分组过滤的字段

    # def has_delete_permission(self):
    #     return

# xadmin.site.register(BusinessIntroduce, PageManageAdmin)


xadmin.site.register(WashCar, PageManageServiceAdmin)
# xadmin.site.register(TechDevp, PageManageServiceAdmin)
xadmin.site.register(Advertise, PageManageServiceAdmin)
xadmin.site.register(CompanyIntroduce, CompanyIntroduceAdmin)   # 公司简介
xadmin.site.register(CompanyService, CompanyServiceAdmin)
# xadmin.site.register(ImgWords, ImgWordsAdmin)
xadmin.site.register(QianQianNumber, QianQianNumberAdmin)