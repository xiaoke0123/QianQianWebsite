from django.shortcuts import render, redirect, HttpResponse
import xadmin
from xadmin import views

from .models import ManageImg, Partner
from django.contrib.auth.models import AbstractUser, Group


# 轮播图管理
class ManageImgAdmin(object):
    # 检索字段
    # search_fields = ['id', 'image_PCImg',  'position', 'url' ]
    # 要显示的字段
    list_display = ['image_pc_img', 'index', 'position', 'url']
    list_editable = ['image_pc_img', 'index', 'position', 'url']
    ordering = ('-index',)
    refresh_times = [3, 5]  # 计时刷新
    list_per_page = 5

    # fk_fields 设置显示外键字段
    # fk_fields = ('cVoteType',)
    # 当一些字段只适合查看的时候，就需要在模型注册类中做出如下添加
    # readonly_fields = ['fav_nums', 'click_num', 'students']
    # exclude = ['', ]  # 不显示字段


class PartnerAdmin(object):
    list_display = ['image_partner','partner', 'index', 'position', ]  # , 'image_MobileImg', 'image_PCImg'
    list_editable = ['partner', 'image_partner', 'position', 'index']
    refresh_times = [3, 5]  # 计时刷新
    list_per_page = 20
    ordering = ('index',)
    # search_fields = ['id', 'position', 'index']
    # list_filter = ['id', 'position', 'index']
    # exclude = ['', ]  # 不显示字段


xadmin.site.register(ManageImg, ManageImgAdmin)
xadmin.site.register(Partner, PartnerAdmin)

