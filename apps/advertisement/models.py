# from django.contrib.auth.models import AbstractUser, Group
from django.db import models
# from django.conf import settings
from django.shortcuts import render, redirect, HttpResponse
# from datetime import datetime
from stdimage.models import StdImageField
from stdimage.utils import UploadToUUID


# """轮播管理"""
class ManageImg(models.Model):
    url = models.CharField(max_length=30, verbose_name='url')
    pc_img = StdImageField(max_length=100, verbose_name="PC轮播图", null=True, blank=True,
                           upload_to=UploadToUUID(path='advertisement/images'),
                           variations={'thumbnail': {'width': 100, 'height': 75}})
    index = models.SmallIntegerField(default=1, verbose_name=u"轮播顺序")

    mobile_img = StdImageField(max_length=100, verbose_name="移动轮播图", null=True, blank=True,
                               upload_to=UploadToUUID(path='advertisement/images'),
                               variations={'thumbnail': {'width': 100, 'height': 75}})
    position = models.CharField(max_length=10, verbose_name='位置', default='首页')

    class Meta:
        # db_table = 'ManageImg'
        db_table = 'manageimg'
        verbose_name = '轮播管理'
        verbose_name_plural = verbose_name

    def image_pc_img(self):
        if self.pc_img:
            return str('<img src="%s" />' % self.pc_img.thumbnail.url)
        else:
            return u'上传图片'

    image_pc_img.short_description = 'PC轮播图'
    image_pc_img.allow_tags = True

    def image_mobile_img(self):
        if self.mobile_img:
            return str('<img src="%s" />' % self.mobile_img.thumbnail.url)
        else:
            return u'上传图片'

    image_mobile_img.short_description = '移动轮播图'
    image_mobile_img.allow_tags = True

    # def __str__(self):
    #     return '{0}(位于第{1}位)'.format(self.title, self.index)


# """合作伙伴"""
class Partner(models.Model):
    partner = models.CharField(max_length=20, verbose_name='合作伙伴', null=True, blank=True)
    recommend = models.SmallIntegerField(choices=((1, '广告设计'), (2, '公司简介')), verbose_name='推荐页面')
    partner_img = StdImageField(max_length=100, verbose_name=u"合作伙伴图",
                                upload_to=UploadToUUID(path='advertisement/images'),
                                variations={'thumbnail': {'width': 100, 'height': 75}}, null=True, blank=True)
    index = models.SmallIntegerField(verbose_name='排序', default=1, null=True, blank=True)
    position = models.CharField(max_length=10, verbose_name='位置', default='首页')

    def image_partner(self):
        if self.partner_img:
            return str('<img src="%s" />' % self.partner_img.thumbnail.url)
        else:
            return u'上传图片'

    image_partner.short_description = '合作伙伴LOGO'
    image_partner.allow_tags = True

    # def __str__(self):
    #     return '{0}(位于第{1}位)'.format(self.title, self.index)

    class Meta:
        # db_table = 'Partner'
        db_table = 'partner'
        verbose_name = '合作伙伴'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.partner


# partner_img = StdImageField  图片上传的方式  可以改为这个  upload_to=UploadToUUID(path=datetime.now().strftime('banner/%Y/%m')),