from django.apps import AppConfig


class AdvertisementConfig(AppConfig):
    name = 'apps.advertisement'
    verbose_name = '广告管理'
