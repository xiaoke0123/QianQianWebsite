from django.db import models
from django.contrib.auth.models import AbstractUser, Group
# Create your models here.


# """系统设置之站点配置"""
class SiteConfiguration(models.Model):
    web_name = models.CharField(max_length=20, verbose_name='网站标题')
    company_name = models.CharField(max_length=20, verbose_name='公司名称')
    keywords = models.CharField(max_length=40, verbose_name='网站关键词', null=True, blank=True)
    desc = models.CharField(max_length=300, null=True, blank=True, verbose_name='网站描述')
    logo = models.ImageField(max_length=300, null=True, blank=True, verbose_name='平台logo')
    address = models.CharField(max_length=20, verbose_name='总公司地址', null=True, blank=True)
    address_branch = models.TextField(max_length=20, verbose_name='分公司地址', null=True, blank=True)
    phone = models.CharField(max_length=20, verbose_name='联系电话', null=True, blank=True)
    applet_img = models.ImageField(upload_to='system', null=True, blank=True, verbose_name='小程序二维码')
    WeChat_img = models.ImageField(upload_to='system', null=True, blank=True, verbose_name='微信公众号二维码')
    customer_service = models.ImageField(upload_to='system', null=True, blank=True, verbose_name='微信客服二维码')
    statistical_code = models.TextField(max_length=80, verbose_name='统计代码', null=True, blank=True)
    copyright = models.TextField(max_length=80, verbose_name='版权信息', null=True, blank=True)

    class Meta:
        # db_table = 'SiteConfiguration'
        db_table = 'siteconfiguration'
        verbose_name = '站点配置'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.web_name


# '''系统设置之平台管理员'''
class User(AbstractUser):
    role = models.CharField(max_length=20, verbose_name=u'角色', null=True, blank=True)
    nick_name = models.CharField(max_length=20, verbose_name=u'昵称', null=True, blank=True)
    department = models.CharField(max_length=20, verbose_name=u'部门', null=True, blank=True)
    cellphone = models.CharField(max_length=11, verbose_name=u'手机号', null=True, blank=True)
    status = models.SmallIntegerField(default=1, choices=((0, '正常'), (1, '禁用-')), verbose_name='状态')
    created_time = models.DateTimeField(verbose_name=u'创建时间',  auto_now_add=True)
    last_login_time = models.DateTimeField(verbose_name=u'最后登录时间', auto_now=True)
    operate_right = models.SmallIntegerField(default=0, choices=((0, '页面管理'), (1, '合作案例管理'),
                                                                 (2, '广告管理'), (3, '新闻资讯管理'), (4, '合作案例管理'),
                                                                 (5, '表单管理'), (6, '表单管理'), (7, '底部导航')),
                                             verbose_name='操作权限')

    class Meta:
        db_table = 'user'
        verbose_name = '平台管理员'
        verbose_name_plural = verbose_name


