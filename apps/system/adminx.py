# from extra_apps import xadmin
# from extra_apps.xadmin import views
from django.shortcuts import render, redirect, HttpResponse
import xadmin
from xadmin import views
from .models import User,  SiteConfiguration


# from apps.page_manage.models import BusinessIntroduce

# Register your models here.


# """系统设置之站点配置"""
class SiteConfigurationAdmin(object):
    # model_icon = 'fa fa-envelope'
    list_per_page = 20
    show_bookmarks = False  # 关闭书签
    # show_bookmarks = True  # 关闭书签
    list_display = ['web_name', 'logo', 'phone']
    refresh_times = [3, 5]  # 计时刷新
    # list_editable = ['title']


xadmin.site.register(SiteConfiguration, SiteConfigurationAdmin)


# """xadmin的基本配置"""
class BaseSetting(object):
    enable_themes = True    # 开启主题切换功能
    use_bootswatch = True    # 显示更多主题


xadmin.site.register(views.BaseAdminView, BaseSetting)


# 全局设置
class GlobalSettings(object):
    menu_style = 'accordion'  # 设置左侧菜单  折叠样式
    site_title = "钳钳后台管理系统"  # 设置站点标题
    site_footer = "钳钳科技有限公司"  # 设置站点的页脚
    menu_style = "accordion"  # 设置菜单折叠


xadmin.site.register(views.CommAdminView, GlobalSettings)













# 用户的后台管理
# class UserAdmin(object):
#     # 检索字段
#     search_fields = ['uName', 'uIP']
#     # 要显示的字段
#     list_display = ['id', 'uName', 'uIP', 'uEmail', 'uNickName', 'uGender', 'uAge', 'uIcon', 'isDelete']
#     # 分组过滤的字段
#     list_filter = ['uName', 'uNickName', 'uGender', 'uAge', 'isDelete']
#     # ordering设置默认排序字段，负号表示降序排序
#     ordering = ('id',)
#     # list_per_page设置每页显示多少条记录，默认是100条
#     list_per_page = 50
#     # list_editable 设置默认可编辑字段
#     list_editable = ['uNickName', 'uIcon']
#
#     list_per_page = 10
#     list_export = ('xls',)
#     list_export_fields = ('id', "username", "phone", "create_time")  # 暂时无用
#     show_bookmarks = False  # 关闭书签
#     list_display = ['id', 'username', 'preview', 'phone', 'balance', 'get_discount', 'all_point']
#
#     def preview(self, obj):
#         return '<img src="%s" height="50" width="50" />' % (obj.head_img)
#     preview.allow_tags = True
#     preview.short_description = "用户头像"
#
#     search_fields = ['name', ]
#     list_editable = ['balance', 'integral']


# 候选者的后台管理
# class CandidateAdmin(object):
#     # 检索字段
#     search_fields = ['cName', 'cAge', 'cTimes', 'cVotes', 'cPinyin', 'isDelete', ]
#     # 要显示的字段
#     list_display = ['id', 'cName', 'cAge', 'cEmail', 'cDeclaration', 'cIcon', 'cTimes', ]
#     # 分组过滤的字段
#     list_filter = ['cName', 'cAge', 'cTimes', 'cVotes', 'cPinyin', 'isDelete', ]
#     # ordering设置默认排序字段，负号表示降序排序
#     ordering = ('id',)
#     # fk_fields 设置显示外键字段
#     fk_fields = ('cVoteType',)
#     # 当一些字段只适合查看的时候，就需要在模型注册类中做出如下添加
#     readonly_fields = ['fav_nums', 'click_num', 'students']
#     list_editable = ['degree', 'desc']  # 列表页直接编辑修改
#     exclude = ['', ]  # 不显示字段
#     refresh_times = [3, 5]  # 计时刷新
















# class GlobalSetting(object):
#     # 菜单设置
#     def get_site_menu(self):
#         return (
#             {'title': '投票管理', 'perm': self.get_model_perm(Poll, 'change'), 'menus': (
#                 {'title': '投票', 'url': self.get_model_url(Poll, 'changelist')},
#                 {'title': '选票', 'url': self.get_model_url(Choice, 'changelist')}
#             )},
#         )
# xadmin.site.register(views.CommAdminView, GlobalSetting)

# 用户的后台管理
# class UserAdmin(object):
#     # 检索字段
#     # search_fields = ['uName', 'uIP',]
#     # 分组过滤的字段
#     # list_filter = ['uName', 'uNickName', 'uGender', 'uAge', 'isDelete']
#     # ordering设置默认排序字段，负号表示降序排序
#     # ordering = ('id',)
#     # list_per_page设置每页显示多少条记录，默认是100条
#     list_per_page = 30
#     # list_editable 设置默认可编辑字段
#     # list_editable = ['uNickName', 'uIcon']
#     # list_export = ('xls',)
#     # list_export_fields = ('id', "username", "phone", "create_time")  # 暂时无用
#     show_bookmarks = False  # 关闭书签
#     list_display = ['username', 'role', 'created_time', 'last_login_time', 'status']
#     # readonly_fields = ['fav_nums', 'click_num', 'students']
#     # exclude = ['', ]  # 不显示字段
#     fields = ['username', 'role', 'password', 'status' ]  # 显示字段
#     search_fields = ['name', ]
#     list_editable = ['balance', 'integral']


# xadmin.site.unregister(User)  # 注销,默认的
# xadmin.site.register(User, UserAdmin)


# def get_site_menu(self):
#     return (
#         {
#             'title': '器材管理',
#             'perm': self.get_model_perm(BusinessIntroduce, 'change'),
#             'icon': 'fa fa-bars',
#             'menus': (
#                 {
#                     'title': '维护记录',
#                     'icon': 'fa fa-bars',
#                     'url': self.get_model_url(BusinessIntroduce, 'changelist')
#                 },
#                 {
#                     'title': '存放位置',
#                     'icon': 'fa fa-archive',
#                     'url': self.get_model_url(BusinessIntroduce, 'changelist')
#                 }
#             )
#         },
#     )

