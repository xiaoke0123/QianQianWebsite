from django.db import models
# Create your models here.
from django.conf import settings
# from DjangoUeditor.models import UEditorField


# """首页之合作案例"""
class CaseClassify(models.Model):
    industry_choices = models.SmallIntegerField(default=1, choices=((1, '服务分类'), (2, '行业分类')), verbose_name='案例类别')
    types = models.CharField(max_length=20, verbose_name='分类名称', unique=True)

    class Meta:
        # db_table = 'CaseClassify'
        db_table = 'caseclassify'
        verbose_name = '案例分类'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.types


# """案例列表"""
class CaseList(models.Model):
    case_name = models.CharField(max_length=20, verbose_name='案例名称')
    company_name = models.CharField(max_length=20, verbose_name='公司名称', null=True, blank=True)
    case_index = models.SmallIntegerField(default=1, verbose_name='案例排序', null=True, blank=True)
    case_time = models.DateField(verbose_name='案例时间', auto_now_add=True, null=True, blank=True)
    service_type = models.ForeignKey(default=1, to='CaseClassify', verbose_name='服务类型', related_name='service')
    industry_classify = models.ForeignKey(default=2, to='CaseClassify', verbose_name='行业分类', related_name='industry')
    recommend = models.BooleanField(default=False, verbose_name='推荐首页')
    img = models.ImageField(upload_to='cooperate_case', verbose_name='封面图', null=True, blank=True)
    function_dsc = models.CharField(max_length=20, verbose_name='功能', null=True, blank=True)
    introduce = models.CharField(max_length=20, verbose_name='设计说明', null=True, blank=True)
    pc_img_01 = models.ImageField(upload_to='cooperate_case', verbose_name='PC端详情图_01', null=True, blank=True)
    mobile_img_01 = models.ImageField(upload_to='cooperate_case', verbose_name='移动端详情图_01', null=True, blank=True)
    pc_img_02 = models.ImageField(upload_to='cooperate_case', verbose_name='PC端详情图_02', null=True, blank=True)
    mobile_img_02 = models.ImageField(upload_to='cooperate_case', verbose_name='移动端详情图_02', null=True, blank=True)
    pc_img_03 = models.ImageField(upload_to='cooperate_case', verbose_name='PC端详情图_03', null=True, blank=True)
    mobile_img_03 = models.ImageField(upload_to='cooperate_case', verbose_name='移动端详情图_03', null=True, blank=True)
    pc_img_04 = models.ImageField(upload_to='cooperate_case', verbose_name='PC端详情图_04', null=True, blank=True)
    mobile_img_04 = models.ImageField(upload_to='cooperate_case', verbose_name='移动端详情图_04', null=True, blank=True)

    class Meta:
        # db_table = 'CaseLIst'
        db_table = 'caselIst'
        verbose_name = '案例列表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.case_name

