import xadmin
from xadmin import views
from .models import CaseClassify, CaseList


class CaseClassifyAdmin(object):
    # model_icon = 'fa fa-envelope'
    list_per_page = 30
    show_bookmarks = False  # 关闭书签
    list_display = ['id', 'types', 'industry_choices']
    # search_fields = ['type', 'service_type', 'industry_classify']
    # 分组过滤的字段
    list_filter = ['industry_choices', 'type',]
    refresh_times = [3, 5]  # 计时刷新
    ordering = ('industry_choices', 'id',)
    list_editable = ['types', 'industry_choices']


xadmin.site.register(CaseClassify, CaseClassifyAdmin)


class CaseListAdmin(object):
    # model_icon = 'fa fa-envelope'  # 图标
    list_per_page = 30
    show_bookmarks = False  # 关闭书签
    refresh_times = [3, 5]  # 计时刷
    style_fields = {'pc_case_detail': 'ueditor', 'mobile_case_detail': 'ueditor'}
    # 检索字段
    search_fields = ['case_name', 'service_type', 'industry_classify']
    # 分组过滤的字段
    list_filter = ['service_type', 'industry_classify' , 'recommend']
    ordering = ('service_type', 'industry_classify', 'case_index', 'case_time')   # ordering设置默认排序字段，负号表示降序排序
    list_display = ['id', 'case_name', 'case_index', 'case_time', 'service_type', 'industry_classify']
    list_editable = ['case_name', 'case_time', 'service_type', 'industry_classify', 'case_index']

    # 服务分类的多对多字段过滤
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'service_type':
            kwargs["queryset"] = CaseClassify.objects.filter(industry_choices=1)
        elif db_field.name == 'industry_classify':
            kwargs["queryset"] = CaseClassify.objects.filter(industry_choices=2)
        return super(CaseListAdmin, self).formfield_for_dbfield(db_field, **kwargs)


xadmin.site.register(CaseList, CaseListAdmin)




