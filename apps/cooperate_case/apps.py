from django.apps import AppConfig


class CooperateCaseConfig(AppConfig):
    name = 'cooperate_case'
    verbose_name = '合作案例'


# class CaseList(AppConfig):
#     name = 'CaseList'
#     verbose_name = '案例列表'