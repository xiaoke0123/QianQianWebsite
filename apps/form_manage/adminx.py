# from extra_apps import xadmin
# from extra_apps.xadmin import views
from django.shortcuts import render, redirect, HttpResponse
import xadmin
from xadmin import views

from .models import FormsManage   # CostAssessment, WashPartner


# 表单管理
class FormsManageAdmin(object):
    list_display = ['id', 'type', 'name', 'status', 'telephone', 'content', 'commit_time', 'remarks',
                    ]    # 要显示的字段   'content1',  'service', 'budget' 'content1'
    list_filter = ['type', 'status', 'service', ]    # 分组过滤的字段
    ordering = ('id',)     # ordering设置默认排序字段，负号表示降序排序
    list_per_page = 30     #  list_per_page设置每页显示多少条记录，默认是100条
    list_editable = ['status', 'remarks']       # list_editable 设置默认可编辑字段
    show_bookmarks = False  # 关闭书签
    search_fields = ['name', ]
    fields = ['status', 'remarks']


xadmin.site.register(FormsManage, FormsManageAdmin)
