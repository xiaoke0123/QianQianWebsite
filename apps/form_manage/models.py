from django.db import models
from django.contrib.auth.models import AbstractUser, Group
from django.db import models
# Create your models here.

from django.conf import settings
from django.shortcuts import render, redirect, HttpResponse
from cooperate_case.models import CaseList


# """表单管理"""
class FormsManage(models.Model):
    type = models.SmallIntegerField(choices=((1, '需求意向表'), (2, '费用评估表'), (3, '洗车合伙人')), verbose_name='表类型')
    company = models.CharField(max_length=20, verbose_name='公司名称', null=True, blank=True)
    name = models.CharField(max_length=10, verbose_name='姓名')
    telephone = models.CharField(max_length=20, verbose_name='电话')
    commit_time = models.DateTimeField(auto_now_add=True, verbose_name='时间')
    service = models.SmallIntegerField(choices=((1, 'APP开发及解决方案'), (2, '网站建设'), (3, '微信营销小程序'),
                                                (4, '电商及系统平台开发'), (5, '年度创意设计服务品'), (6, '牌策划视觉设计'),
                                                (7, '数字整合营销传播'), (8, '社会化营销传播')), default=0,
                                       verbose_name='服务类型', null=True, blank=True)
    budget = models.SmallIntegerField(choices=((0, '其他'), (1, '3-5万'), (2, '5-8万'), (3, '8-10万'), (4, '10万以上')),
                                      default=0, verbose_name='预算', null=True, blank=True)
    # content1 = models.CharField(max_length=200, null=True, blank=True, verbose_name='客户的选择1')
    content = models.TextField(max_length=200, null=True, blank=True, verbose_name='客户的选择')
    desc = models.CharField(max_length=200, null=True, blank=True, verbose_name='描述')
    email = models.CharField(max_length=20, verbose_name='邮箱', null=True, blank=True)
    status = models.SmallIntegerField(default=0, choices=((0, '未处理'), (1, '已处理')), verbose_name='审核状态')
    remarks = models.CharField(max_length=60, verbose_name='备注', null=True, blank=True)

    # need = models.SmallIntegerField(choices=((1, '企业品牌设计'), (2, '产品品牌设计'), (3, '店铺品牌设计'),
    #                                          (4, '其他'),),
    #                                 default=1, verbose_name='需要的服务类型', null=True, blank=True)
    # want = models.SmallIntegerField(choices=((1, '品牌策划'), (2, 'Vi设计'), (3, 'LOGO设计'),
    #                                          (4, '折页设计'), (5, '画册设计'), (6, '包装设计')),
    #                                 default=0, verbose_name='服务类型', null=True, blank=True)

    # want = models.ForeignKey(to=CaseList, default=1, verbose_name='想要的服务类型', null=True, blank=True)

    request = models.CharField(max_length=50, verbose_name='一句话需求描述', null=True, blank=True)

    class Meta:
        # db_table = 'FormsManage'
        db_table = 'formsmanage'
        verbose_name = '表单管理'
        verbose_name_plural = verbose_name

    # def __str__(self):
    #     return self.type

