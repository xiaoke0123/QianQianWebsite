from django.apps import AppConfig


class FormManageConfig(AppConfig):
    name = 'form_manage'
    verbose_name = '表单管理'
