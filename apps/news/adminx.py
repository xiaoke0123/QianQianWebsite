# from extra_apps import xadmin
# from extra_apps.xadmin import views
from django.shortcuts import render, redirect, HttpResponse
import xadmin
from xadmin import views

from .models import InfoClassify,  InfoList

# Register your models here.


# """资讯分类"""
class InfoClassifyAdmin(object):
    # model_icon = 'fa fa-envelope'
    list_per_page = 20
    show_bookmarks = False  # 关闭书签
    # show_bookmarks = True  # 关闭书签
    list_display = ['id', 'category_level', 'category_name']
    # refresh_times = [3, 5]  # 计时刷新
    list_editable = ['category_level', 'category_name']
    ordering = ('category_level',)

    # 服务分类的多对多字段过滤
    # def formfield_for_dbfield(self, db_field, **kwargs):
    #     if db_field.name == 'service_type':
    #         kwargs["queryset"] = CaseClassify.objects.filter(industry_choices=1)
    #     elif db_field.name == 'industry_classify':
    #         kwargs["queryset"] = CaseClassify.objects.filter(industry_choices=2)
    #     return super(CaseListAdmin, self).formfield_for_dbfield(db_field, **kwargs)


    # del_view.short_description = '商品名称'
xadmin.site.register(InfoClassify, InfoClassifyAdmin)


# """资讯列表"""
class InfoListAdmin(object):
    # model_icon = 'fa fa-envelope'
    list_per_page = 20
    show_bookmarks = False  # 关闭书签
    # show_bookmarks = True  # 关闭书签
    list_display = ['id', 'title', 'category', 'recommend_index', 'clicks']
    refresh_times = [3, 5]  # 计时刷新
    list_editable = ['id', 'title', 'category', 'recommend_index', 'clicks']
    ordering = ('category', )


xadmin.site.register(InfoList, InfoListAdmin)













# # 候选者的后台管理
# class CandidateAdmin(object):
#     # 检索字段
#     # search_fields = ['cName', 'cAge', 'cTimes', 'cVotes', 'cPinyin', 'isDelete', ]
#     # 要显示的字段
#     # list_display = ['id', 'cName', 'cAge', 'cEmail', 'cDeclaration', 'cIcon', 'cTimes', 'cVotes', 'cPinyin', 'isDelete', 'cVoteType',]
#     # 分组过滤的字段
#     # list_filter = ['cName', 'cAge', 'cTimes', 'cVotes', 'cPinyin', 'isDelete', ]
#     # ordering设置默认排序字段，负号表示降序排序
#     ordering = ('id',)
#     # fk_fields 设置显示外键字段
#     fk_fields = ('cVoteType',)
#     # 当一些字段只适合查看的时候，就需要在模型注册类中做出如下添加
#     readonly_fields = ['fav_nums', 'click_num', 'students']
#     list_editable = ['degree', 'desc']  # 列表页直接编辑修改
#     exclude = ['', ]  # 不显示字段
#     refresh_times = [3, 5]  # 计时刷新

