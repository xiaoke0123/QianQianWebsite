from django.db import models
# Create your models here.
from apps.page_manage.models import PageManageBaseModel


# """资讯分类"""
class InfoClassify(models.Model):
    # category = models.SmallIntegerField(choices=((1, '公告'), (2, '行业新闻')), verbose_name='分类名称')
    # category = models.CharField(max_length=20, verbose_name='资讯标题')

    category_level = models.SmallIntegerField(choices=((1, '一级分类'), (2, '二级分类')), verbose_name='分类名称', default=1,)
    category_name = models.CharField(max_length=20, verbose_name='分类名称', default='行业新闻')

    class Meta:
        # db_table = 'InfoClassify'
        db_table = 'infoclassify'
        verbose_name = '资讯分类'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.category_name


# """资讯列表"""
class InfoList(PageManageBaseModel):
    # title = models.CharField(max_length=40, verbose_name='标题')
    # keywords = models.CharField(max_length=40, verbose_name='关键词', null=True, blank=True)
    # desc = models.CharField(max_length=300, null=True, blank=True, verbose_name='描述')
    # category = models.SmallIntegerField(choices=((1, '公告'), (2, '行业新闻')), verbose_name='分类名称')
    content = models.TextField(verbose_name='文本内容', null=True, blank=True)  # max_length=80,
    category = models.ForeignKey('InfoClassify', verbose_name='分类名称', default=1)
    img = models.ImageField(upload_to='news', verbose_name='封面图', null=True, blank=True)
    img_01 = models.ImageField(upload_to='news', verbose_name='辅助图_01', null=True, blank=True)
    img_02 = models.ImageField(upload_to='news', verbose_name='辅助图_02', null=True, blank=True)
    commit_time = models.DateField(auto_now_add=True)
    recommend_index = models.SmallIntegerField(default=0, verbose_name='推荐排序', null=True, blank=True)
    clicks = models.IntegerField(default=0, verbose_name='点击量', null=True, blank=True)
    author = models.CharField(max_length=10, verbose_name='作者', default='吴彦祖')

    class Meta:
        # db_table = 'InfoList'
        db_table = 'infolist'
        verbose_name = '资讯列表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title


