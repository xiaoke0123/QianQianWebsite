"""QianQianWebsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""


from django.conf.urls import url, include
from extra_apps import xadmin


from django.views.static import serve
# from django.conf.settings import MEDIA_ROOT   TemplateView
from django.conf import settings

from apps.page_manage.urls import urlpatterns as page_manage_url
from django.views.generic.base import TemplateView

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^xadmin/', xadmin.site.urls),

    url('media/(?P<path>.*)', serve, {"document_root": settings.MEDIA_ROOT}),

    url(r'^page_manage/', include((page_manage_url, "page_manage"), namespace="page_manage")),
    url(r'^$', TemplateView.as_view(template_name="index.html")),

    url(r'^static/(?P<path>.*)$', serve, {"document_root": settings.STATICFILES_DIRS[0]}),


]
